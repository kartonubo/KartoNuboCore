# KartoNuboCore

Das Core-Modul des zentralen Services KartoServo über den die Karten verwaltet werden.

Weiter Details zum Projekt KartoNubo befinden sich auf der Webseite http://www.kartonubo.org/ .


## Weitere Dokumente

* [ToDo's](TODO.md)
* [OrientDB](OrientDB.md)


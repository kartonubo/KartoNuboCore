# OrientDB installieren

TODO

# RemoteDB anlegen

TODO

# LocaleTestDB anlegen

**1) OrientDB-Console aufrufen**

<pre>
orientdb-community-2.0.10\bin>console.bat
</pre>

**2) DB erstellen**

<pre>
orientdb> CREATE DATABASE plocal:D:/Entwicklung/Projekte/KartoNubo/Projekte/KartoNuboCore/OrientDB/localOrientTestDB
 
Creating database [plocal:D:/Entwicklung/Projekte/KartoNubo/Projekte/KartoNuboCore/OrientDB/localOrientTestDB] using the
 storage type [plocal]...
Database created successfully.
 
Current database is: plocal:D:/Entwicklung/Projekte/KartoNubo/Projekte/KartoNuboCore/OrientDB/localOrientTestDB
orientdb {db=localOrientTestDB}>
</pre>

**3) Console schließen**
<pre>
orientdb {db=localOrientTestDB}> exit
</pre>
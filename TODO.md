# ToDo's

## Implementierung CardRepository

* **OrientDB auf Version 2.2.2 updaten** 
    * Warten auf orientdb-enterprise (aktuell 20.Juni.2016 nur 2.1.19)
* **Interface einführen**
    * CardRepository in CardRepositoryImpl umbenennen
    * Interface CardRepository incl. Javadoc erstellen
* **Methode find implementieren**
    * Folgende SucheValues implementieren
        * GROUP_ID             (impl.)
		* ARTIFACT_ID          (impl.) 
		* NAME                 (impl.)
		* COMMENT
		* TEXT
		* LAST_UPDATE 
		* TAG_LIST      ???
    * Operatoren
        * EQUALS("="),             (impl.)
        * NOT_EQUALS("<>"),        (impl. NOTEST) 
        * LESSTHAN("<"),           (impl. NOTEST)
        * LESSTHAN_EQUAL("<="),    (impl. NOTEST)
        * GREATERTHAN(">"),        (impl. NOTEST)
        * GREATERTHAN_EQUAL(">="), (impl. NOTEST)
	    * BETWEEN("BETWEEN"),       
	    * IN("IN"),                (impl.)
	    * IS_NULL("IS NULL"), 
	    * MATCHES("MATCHES"), 
	    * LIKE("LIKE")             (impl. NOTEST)
    * LinkOperatoren
         * AND         (impl.)
         * OR          (impl. NOTEST)
         * AND_NOT     (impl. NOTEST)
         * OR_NOT      (impl. NOTEST)
	* UnitTest incl. Kombinationen erstellen
        
* **Deprecated Methode setLimit durch "SQL LIMIT" ersetzen**       
 
* **JavaDoc**

## Card-Klasse

* JavaDoc
* JunitTests ausbauen

## Implementierung Tag (incl. Edge)

* PeformanceVergleich mit String-Implementierung in Card-Klasse

## Implementierung Releation


/*
 *  Copyright 2016 Thomas Heynk
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package de.kartonubo.core.data;

import de.kartonubo.core.exception.ValueValidateException;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author Thomas
 */
public class CardTest
{

    @Test
    public void testCardTags()
    {
        Card testCard;

        try
        {
            testCard = new Card( "group.id", "artefactId" );
            testCard.addTag( "hallo" );
            assertTrue( "addTag hallo", testCard.isTagDefined( "hallo" ) );
            testCard.addTag( "hallo" );
            testCard.addTag( "hallo_thomas" );
            assertTrue( "addTag 'hallo_thomas'", testCard.isTagDefined( "hallo_thomas" ) );
            testCard.removeTag( "hallo" );
            assertFalse( "removeTag hallo", testCard.isTagDefined( "hallo" ) );
            assertTrue( "removeTag hallo not 'hallo_thomas'", testCard.isTagDefined( "hallo_thomas" ) );
        }
        catch ( ValueValidateException ex )
        {
            ex.printStackTrace();
            fail( "TestCard " );
            return;
        }

        try
        {
            testCard.addTag( "abc-def" );
        }
        catch ( ValueValidateException ex )
        {
            fail( "Tag abc-def" );
        }

        try{
            String orgTagList = testCard.getTagList();
            testCard.setTagList( orgTagList );
            assertEquals("SetTagList", orgTagList, testCard.getTagList());
        }
        catch ( ValueValidateException ex )
        {
            fail( "SetTagList" );
        }
        
        try
        {
            testCard.addTag( "Hallo Thomas" );
            fail( "Tag 'Hallo Thomas" );
        }
        catch ( ValueValidateException ex )
        {
        }
    }

    @Test
    public void testValidFunktions()
    {
        assertTrue( "vG1", Card.isGroupIdValid( "abwjs-adfh" ) );
        assertTrue( "vG2", Card.isGroupIdValid( "abw.jsa.dfh" ) );
        assertFalse( "vG3", Card.isGroupIdValid( ".bw.jsa.dfh" ) );
        assertFalse( "vG4", Card.isGroupIdValid( "bw.jsa.dfh." ) );
        assertFalse( "vG5", Card.isGroupIdValid( "bw.jAa.dfh" ) );
        assertFalse( "vG5", Card.isGroupIdValid( "biu.j1" ) );

        assertTrue( "vA1", Card.isArtifactIdValid( "tomHey" ) );

        assertTrue( "v1tag", Card.isTagValid( "iA" ) );
        assertFalse( "v2tag", Card.isTagValid( "i" ) );
        assertTrue( "v3tag", Card.isTagValid( "iÜ1" ) );
    }

}

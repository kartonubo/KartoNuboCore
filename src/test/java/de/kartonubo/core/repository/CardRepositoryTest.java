/*
 *  Copyright 2016 Thomas Heynk
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package de.kartonubo.core.repository;

import de.kartonubo.core.exception.RepositoryException;
import de.kartonubo.core.data.Card;
import de.kartonubo.core.service.ConfigurationManager;
import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

import java.util.List;

/**
 * @author Thomas
 */
public class CardRepositoryTest
{

    static CardRepository repo;

    public CardRepositoryTest()
    {
    }

    @BeforeClass
    public static void setUpClass()
    {
        try
        {
            repo = new CardRepository();
        }
        catch ( RepositoryException e )
        {
            fail( "New CardRepository" );
            return;
        }
    }

    @AfterClass
    public static void tearDownClass()
    {
        if ( repo != null )
        {
            repo.closeAll();
        }
    }

    @Before
    public void setUp()
    {
        ConfigurationManager.getInstance().setOrientDatabaseURLtoMEMORY();
        // ConfigurationManager.getInstance().setOrientDatabaseURLtoLOCAL();
    }

    @After
    public void tearDown()
    {
    }

    /**
     * Test of createCard method, of class CardRepository.
     */
    @Test
    public void testCreateGetDelCard_String_String()
    {
        // (1) CardCreate
        Card cardCreate = null;
        try
        {
            cardCreate = repo.createCard( "grp.heynk", "thomas" );
        }
        catch ( RepositoryException ex )
        {
            fail( "CardCreate" );
        }
        checkBaseCard( cardCreate, "CardCreate" );

        // (2) CardCreateError
        Card cardCreateError = null;
        try
        {
            cardCreateError = repo.createCard( "grp.heynk", "thomas" );
            fail( "CardCreateError" );
        }
        catch ( RepositoryException ex )
        {
            Assert.assertEquals( "Exception: TYPE_CARD_EXISTS", RepositoryException.CARD_EXISTS, ex.getType() );
        }
        assertNull( cardCreateError );

        // (3) CardGet
        Card cardGet = null;
        try
        {
            cardGet = repo.getCard( "grp.heynk", "thomas" );
        }
        catch ( RepositoryException ex )
        {
            fail( "CardGet" );
        }
        checkBaseCard( cardGet, "CardGet" );

        // (4) CardDelete
        try
        {
            repo.deleteCard( "grp.heynk", "thomas" );
        }
        catch ( RepositoryException ex )
        {
            fail( "CardDelete" );
        }

        // (5) CardGetDel
        Card cardGetDel = null;
        try
        {
            cardGetDel = repo.getCard( "grp.heynk", "thomas" );
        }
        catch ( RepositoryException ex )
        {
            Assert.assertEquals( "Exception: TYPE_CARD_NOT_EXISTS", RepositoryException.CARD_NOT_EXISTS, ex.getType() );
        }
        assertNull( cardGetDel );
    }

    /**
     *
     */
    @Test
    public void testCreateGetDelCard_Obj()
    {
        Card karte01 = CardTestObjects.getKarte01();

        CardRepository repo;
        try
        {
            repo = new CardRepository();
        }
        catch ( RepositoryException e )
        {
            fail( "New CardRepository" );
            return;
        }
        // (1) CardCreate
        Card cardCreate = null;
        try
        {
            cardCreate = repo.createCard( karte01 );
        }
        catch ( RepositoryException ex )
        {
            fail( "CardCreate(karte01)" );
        }
        checkCards( karte01, cardCreate, "CardCreate(karte01)" );

        // (2) CardUpdate
        karte01.setName( "Thomas" );

        Card cardUpdate = null;

        try
        {
            cardUpdate = repo.updateCard( karte01 );
        }
        catch ( RepositoryException ex )
        {
            fail( "CardUpdate(karte01)" );
        }
        checkCards( karte01, cardUpdate, "CardUpdate(karte01)" );
        repo.closeAll();
    }

    @Test
    public void testFind()
    {
        Card karte01 = CardTestObjects.getKarteTest2_01();
        Card karte02 = CardTestObjects.getKarteTest2_02();
        Card karte03 = CardTestObjects.getKarteTest2_03();
        Card karte04 = CardTestObjects.getKarteTest2_04();

        CardRepository repo;
        try
        {
            repo = new CardRepository();
        }
        catch ( RepositoryException e )
        {
            fail( "New CardRepository" );
            return;
        }
        // (1) CreateCards
        try
        {
            repo.createCard( karte01 );
            repo.createCard( karte02 );
            repo.createCard( karte03 );
            repo.createCard( karte04 );
        }
        catch ( RepositoryException ex )
        {
            fail( "CreateCards" );
        }

        // (2) FindByName
        List<Card> findByList = null;
        String msgTeilString = "findByName(" + karte01.getName() + ")";
        try
        {
            findByList = repo.findByName( karte01.getName() );
        }
        catch ( RepositoryException ex )
        {
            fail( msgTeilString );
        }
        assertNotNull( msgTeilString, findByList );
        assertEquals( msgTeilString + " Size", 2, findByList.size() );

        // (3) FindByName
        msgTeilString = "findByName(" + karte03.getName() + ")";
        try
        {
            findByList = repo.findByName( karte03.getName() );
        }
        catch ( RepositoryException ex )
        {
            fail( msgTeilString );
        }
        assertNotNull( msgTeilString, findByList );
        assertEquals( msgTeilString + " Size", 1, findByList.size() );

        // (4) FindByName
        msgTeilString = "findByName(leer)";
        try
        {
            findByList = repo.findByName( "leer" );
        }
        catch ( RepositoryException ex )
        {
            fail( msgTeilString );
        }
        assertNotNull( msgTeilString, findByList );
        assertEquals( msgTeilString + " Size", 0, findByList.size() );

        // (10) FindByGroupId
        msgTeilString = "findByGroupId(" + karte01.getGroupId() + ")";
        try
        {
            findByList = repo.findByGroupId( karte01.getGroupId() );
        }
        catch ( RepositoryException ex )
        {
            fail( msgTeilString );
        }
        assertNotNull( msgTeilString, findByList );
        assertEquals( msgTeilString + " Size", 1, findByList.size() );

        // (11) FindByGroupId
        msgTeilString = "findByGroupId(" + karte03.getGroupId() + ")";
        try
        {
            findByList = repo.findByGroupId( karte03.getGroupId() );
        }
        catch ( RepositoryException ex )
        {
            fail( msgTeilString );
        }
        assertNotNull( msgTeilString, findByList );
        assertEquals( msgTeilString + " Size", 2, findByList.size() );

        // (12) FindByGroupId
        msgTeilString = "findByGroupId(leer)";
        try
        {
            findByList = repo.findByGroupId( "leer" );
        }
        catch ( RepositoryException ex )
        {
            fail( msgTeilString );
        }
        assertNotNull( msgTeilString, findByList );
        assertEquals( msgTeilString + " Size", 0, findByList.size() );

        // (15) FindByArtifactId mit Limit == 2
        msgTeilString = "findByArtifactId(" + karte01.getArtifactId() + ") + Limit(2)";
        try
        {
            int limitOrg = repo.getResultLimit();
            repo.setResultLimit( 2 );
            findByList = repo.findByArtifactId( karte01.getArtifactId() );
            repo.setResultLimit( limitOrg );
        }
        catch ( RepositoryException ex )
        {
            fail( msgTeilString );
        }
        assertNotNull( msgTeilString, findByList );
        assertEquals( msgTeilString + " Size", 2, findByList.size() );

        // (16) FindByArtifactId
        msgTeilString = "findByArtifactId(" + karte01.getArtifactId() + ")";
        try
        {
            findByList = repo.findByArtifactId( karte01.getArtifactId() );
        }
        catch ( RepositoryException ex )
        {
            fail( msgTeilString );
        }
        assertNotNull( msgTeilString, findByList );
        assertEquals( msgTeilString + " Size", 3, findByList.size() );

        // (17) FindByArtifactId
        msgTeilString = "findByArtifactId(" + karte03.getArtifactId() + ")";
        try
        {
            findByList = repo.findByArtifactId( karte03.getArtifactId() );
        }
        catch ( RepositoryException ex )
        {
            fail( msgTeilString );
        }
        assertNotNull( msgTeilString, findByList );
        assertEquals( msgTeilString + " Size", 1, findByList.size() );

        // (18) FindByArtifactId
        msgTeilString = "findByArtifactId(leer)";
        try
        {
            findByList = repo.findByArtifactId( "leer" );
        }
        catch ( RepositoryException ex )
        {
            fail( msgTeilString );
        }
        assertNotNull( msgTeilString, findByList );
        assertEquals( msgTeilString + " Size", 0, findByList.size() );

    }

    //
    // Utils
    //

    private void checkBaseCard( Card baseCard, String name )
    {
        assertNotNull( name + " is Null!", baseCard );
        assertEquals( name + ": Falsche GroupId!", "grp.heynk", baseCard.getGroupId() );
        assertEquals( name + ": Falsche ArtifactId!", "thomas", baseCard.getArtifactId() );
        assertEquals( name + ": Falscher Name!", "", baseCard.getName() );
        assertEquals( name + ": Comment is not Empty", "", baseCard.getComment() );
        assertEquals( name + ": Text is not Empty", "", baseCard.getText() );
        assertEquals( name + ": TagList is not Empty", "", baseCard.getTagList() );
        assertNotNull( name + ": LastUpdate is Null!", baseCard.getLastUpdate() );
    }

    private void checkCards( Card cardIn, Card cardOut, String name )
    {
        assertNotNull( name + ": CardOut is Null!", cardOut );
        assertEquals( name + ": Falsche GroupId!", cardIn.getGroupId(), cardOut.getGroupId() );
        assertEquals( name + ": Falsche ArtifactId!", cardIn.getArtifactId(), cardOut.getArtifactId() );
        assertEquals( name + ": Falscher Name!", cardIn.getName(), cardOut.getName() );
        assertEquals( name + ": Falscher Comment!", cardIn.getComment(), cardOut.getComment() );
        assertEquals( name + ": Falscher Text!", cardIn.getText(), cardOut.getText() );
        assertEquals( name + ": Falsche TagList!", cardIn.getTagList(), cardOut.getTagList() );
        assertNotSame( name + ": LastUpdate!", cardIn.getLastUpdate(), cardOut.getLastUpdate() );
    }

}

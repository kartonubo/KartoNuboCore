/*
 *  Copyright 2016 Thomas Heynk
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package de.kartonubo.core.repository;

import de.kartonubo.core.data.Card;
import de.kartonubo.core.exception.ValueValidateException;
import static org.junit.Assert.*;

/**
 * @author Thomas
 */
public class CardTestObjects
{

    public static Card getKarte01()
    {
        Card karte = null;
        try
        {
            karte = new Card( "first.group", "artifact01" );
        }
        catch ( ValueValidateException ex )
        {
            fail( "getKarte01" );
        }
        return karte;
    }

    public static Card getKarteTest2_01()
    {
        Card karte = null;
        try
        {
            karte = new Card( "find.by.test.a", "artifact02" );
            karte.setName( "Andreas" );
        }
        catch ( ValueValidateException ex )
        {
            fail( "getKarte01" );
        }
        return karte;
    }

    public static Card getKarteTest2_02()
    {
        Card karte = null;
        try
        {
            karte = new Card( "find.by.test.b", "artifact02" );
            karte.setName( "Andreas" );
        }
        catch ( ValueValidateException ex )
        {
            fail( "getKarte01" );
        }
        return karte;
    }

    public static Card getKarteTest2_03()
    {
        Card karte = null;
        try
        {
            karte = new Card( "find.by.test.b", "artifact03" );
            karte.setName( "Birgit" );
        }
        catch ( ValueValidateException ex )
        {
            fail( "getKarte01" );
        }
        return karte;
    }

    public static Card getKarteTest2_04()
    {
        Card karte = null;
        try
        {
            karte = new Card( "find.by.test.c", "artifact02" );
            karte.setName( "Hugo" );
        }
        catch ( ValueValidateException ex )
        {
            fail( "getKarte01" );
        }
        return karte;
    }

    public static Card getKarteTest3_01()
    {
        Card karte = null;
        try
        {
            karte = new Card( "find.by.test.aa", "artifact32" );
            karte.setName( "Andrea" );
        }
        catch ( ValueValidateException ex )
        {
            fail( "getKarte01" );
        }
        return karte;
    }

    public static Card getKarteTest3_02()
    {
        Card karte = null;
        try
        {
            karte = new Card( "find.by.test.bb", "artifact32" );
            karte.setName( "Andrea" );
        }
        catch ( ValueValidateException ex )
        {
            fail( "getKarte01" );
        }
        return karte;
    }

    public static Card getKarteTest3_03()
    {
        Card karte = null;
        try
        {
            karte = new Card( "find.by.test.bb", "artifact33" );
            karte.setName( "Anna" );
        }
        catch ( ValueValidateException ex )
        {
            fail( "getKarte01" );
        }
        return karte;
    }

    public static Card getKarteTest3_04()
    {
        Card karte = null;
        try
        {
            karte = new Card( "find.by.test.cc", "artifact32" );
            karte.setName( "Emil" );
        }
        catch ( ValueValidateException ex )
        {
            fail( "getKarte01" );
        }
        return karte;
    }

}

/*
 *  Copyright 2016 Thomas Heynk
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package de.kartonubo.core.service;

import com.tinkerpop.blueprints.impls.orient.OrientGraph;

import de.kartonubo.core.exception.OrientDBManagerException;

//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author Thomas
 */
public class OrientDBManagerTest
{

    @org.junit.Test
    public void basisTest()
    {
        OrientDBManager manager = new OrientDBManager();

        try
        {
            manager.openFactory();
            assertEquals( true, manager.isFactoryOpen() );
            OrientGraph graph = manager.createGraph();
            assertNotNull( graph );
            assertEquals( false, graph.isClosed() );
            graph.shutdown();
            assertEquals( true, graph.isClosed() );
        }
        catch ( OrientDBManagerException e )
        {
            fail( "OrientDBManagerException" );
        }
        finally
        {
            manager.closeFactory();
        }
    }

}

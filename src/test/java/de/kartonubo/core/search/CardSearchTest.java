/*
 *  Copyright 2016 Thomas Heynk
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package de.kartonubo.core.search;

import de.kartonubo.core.exception.CardSearchDefinitionException;
import de.kartonubo.core.exception.RepositoryException;
import de.kartonubo.core.repository.CardRepository;
import de.kartonubo.core.repository.CardTestObjects;
import de.kartonubo.core.data.Card;
import de.kartonubo.core.service.ConfigurationManager;
import javassist.bytecode.Descriptor.Iterator;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Thomas
 */
public class CardSearchTest
{

    static CardRepository repo;

    public CardSearchTest()
    {
    }

    @BeforeClass
    public static void setUpClass()
    {
        try
        {
            repo = new CardRepository();
        }
        catch ( RepositoryException e )
        {
            fail( "New CardRepository" );
            return;
        }
    }

    @AfterClass
    public static void tearDownClass()
    {
        if ( repo != null )
        {
            repo.closeAll();
        }
    }

    @Before
    public void setUp()
    {
        ConfigurationManager.getInstance().setOrientDatabaseURLtoMEMORY();
        // ConfigurationManager.getInstance().setOrientDatabaseURLtoLOCAL();
    }

    @After
    public void tearDown()
    {
    }

    @Test
    public void testFind()
    {
        Card karte01 = CardTestObjects.getKarteTest3_01();
        Card karte02 = CardTestObjects.getKarteTest3_02();
        Card karte03 = CardTestObjects.getKarteTest3_03();
        Card karte04 = CardTestObjects.getKarteTest3_04();

        // (1) CreateCards
        try
        {
            repo.createCard( karte01 );
            repo.createCard( karte02 );
            repo.createCard( karte03 );
            repo.createCard( karte04 );
        }
        catch ( RepositoryException ex )
        {
            fail( "CreateCards" );
        }
        List<Card> findByList = null;
        String msgTeilString = "";
        CardSearchDefinition searchDef;

        // ----------------------------------------------------------------
        // (2) FindByName
        msgTeilString = "findByName(" + karte01.getName() + ")";
        try
        {
            searchDef = new CardSearchDefinition();
            searchDef.addName( karte01.getName() );
            findByList = repo.find( searchDef );
        }
        catch ( RepositoryException ex )
        {
            fail( "RepositoryEx: " + msgTeilString );
        }
        catch ( CardSearchDefinitionException e )
        {
            fail( "CardSearchEx: " + msgTeilString );
        }
        assertNotNull( msgTeilString, findByList );
        assertEquals( msgTeilString + " Size", 2, findByList.size() );

        // (3) FindByName
        findByList = null;
        msgTeilString = "findByName(" + karte03.getName() + ")";
        try
        {
            searchDef = new CardSearchDefinition();
            searchDef.addName( karte03.getName() );
            findByList = repo.find( searchDef );
        }
        catch ( RepositoryException ex )
        {
            fail( "RepositoryEx: " + msgTeilString );
        }
        catch ( CardSearchDefinitionException e )
        {
            fail( "CardSearchEx: " + msgTeilString );
        }
        assertNotNull( msgTeilString, findByList );
        assertEquals( msgTeilString + " Size", 1, findByList.size() );

        // (4) FindByName
        findByList = null;
        msgTeilString = "findByName(leer)";
        try
        {
            searchDef = new CardSearchDefinition();
            searchDef.addName( "leer" );
            findByList = repo.find( searchDef );
        }
        catch ( RepositoryException ex )
        {
            fail( "RepositoryEx: " + msgTeilString );
        }
        catch ( CardSearchDefinitionException e )
        {
            fail( "CardSearchEx: " + msgTeilString );
        }
        assertNotNull( msgTeilString, findByList );
        assertEquals( msgTeilString + " Size", 0, findByList.size() );

        // ----------------------------------------------------------------
        // (10) FindByGroupId
        findByList = null;
        msgTeilString = "findByGroupId(" + karte01.getGroupId() + ")";
        try
        {
            searchDef = new CardSearchDefinition();
            searchDef.addGroupId( karte01.getGroupId() );
            findByList = repo.find( searchDef );
        }
        catch ( RepositoryException ex )
        {
            fail( "RepositoryEx: " + msgTeilString );
        }
        catch ( CardSearchDefinitionException e )
        {
            fail( "CardSearchEx: " + msgTeilString );
        }
        assertNotNull( msgTeilString, findByList );
        assertEquals( msgTeilString + " Size", 1, findByList.size() );

        // (11) FindByGroupId
        findByList = null;
        msgTeilString = "findByGroupId(" + karte03.getGroupId() + ")";
        try
        {
            searchDef = new CardSearchDefinition();
            searchDef.addGroupId( karte03.getGroupId() );
            findByList = repo.find( searchDef );
        }
        catch ( RepositoryException ex )
        {
            fail( "RepositoryEx: " + msgTeilString );
        }
        catch ( CardSearchDefinitionException e )
        {
            fail( "CardSearchEx: " + msgTeilString );
        }
        assertNotNull( msgTeilString, findByList );
        assertEquals( msgTeilString + " Size", 2, findByList.size() );

        // (12) FindByGroupId
        findByList = null;
        msgTeilString = "findByGroupId(leer)";
        try
        {
            searchDef = new CardSearchDefinition();
            searchDef.addGroupId( "leer" );
            findByList = repo.find( searchDef );
        }
        catch ( RepositoryException ex )
        {
            fail( "RepositoryEx: " + msgTeilString );
        }
        catch ( CardSearchDefinitionException e )
        {
            fail( "CardSearchEx: " + msgTeilString );
        }
        assertNotNull( msgTeilString, findByList );
        assertEquals( msgTeilString + " Size", 0, findByList.size() );

        // ----------------------------------------------------------------
        // (15) FindByArtifactId mit Limit == 2
        findByList = null;
        msgTeilString = "findByArtifactId(" + karte01.getArtifactId() + ") + Limit(2)";
        try
        {
            int limitOrg = repo.getResultLimit();
            repo.setResultLimit( 2 );
            searchDef = new CardSearchDefinition();
            searchDef.addArtifactId( karte01.getArtifactId() );
            findByList = repo.find( searchDef );
            repo.setResultLimit( limitOrg );
        }
        catch ( RepositoryException ex )
        {
            fail( "RepositoryEx: " + msgTeilString );
        }
        catch ( CardSearchDefinitionException e )
        {
            fail( "CardSearchEx: " + msgTeilString );
        }
        assertNotNull( msgTeilString, findByList );
        assertEquals( msgTeilString + " Size", 2, findByList.size() );

        // (16) FindByArtifactId
        findByList = null;
        msgTeilString = "findByArtifactId(" + karte01.getArtifactId() + ")";
        try
        {
            searchDef = new CardSearchDefinition();
            searchDef.addArtifactId( karte01.getArtifactId() );
            findByList = repo.find( searchDef );
        }
        catch ( RepositoryException ex )
        {
            fail( "RepositoryEx: " + msgTeilString );
        }
        catch ( CardSearchDefinitionException e )
        {
            fail( "CardSearchEx: " + msgTeilString );
        }
        assertNotNull( msgTeilString, findByList );
        assertEquals( msgTeilString + " Size", 3, findByList.size() );

        // (17) FindByArtifactId
        findByList = null;
        msgTeilString = "findByArtifactId(" + karte03.getArtifactId() + ")";
        try
        {
            searchDef = new CardSearchDefinition();
            searchDef.addArtifactId( karte03.getArtifactId() );
            findByList = repo.find( searchDef );
        }
        catch ( RepositoryException ex )
        {
            fail( "RepositoryEx: " + msgTeilString );
        }
        catch ( CardSearchDefinitionException e )
        {
            fail( "CardSearchEx: " + msgTeilString );
        }
        assertNotNull( msgTeilString, findByList );
        assertEquals( msgTeilString + " Size", 1, findByList.size() );

        // (18) FindByArtifactId
        findByList = null;
        msgTeilString = "findByArtifactId(leer)";
        try
        {
            searchDef = new CardSearchDefinition();
            searchDef.addArtifactId( "leer" );
            findByList = repo.find( searchDef );
        }
        catch ( RepositoryException ex )
        {
            fail( "RepositoryEx: " + msgTeilString );
        }
        catch ( CardSearchDefinitionException e )
        {
            fail( "CardSearchEx: " + msgTeilString );
        }
        assertNotNull( msgTeilString, findByList );
        assertEquals( msgTeilString + " Size", 0, findByList.size() );

        // ----------------------------------------------------------------
        // (19) FindByName Operator "IN"
        findByList = null;
        msgTeilString = "findByName IN (" + karte03.getName() + ")";
        try
        {
            searchDef = new CardSearchDefinition();
            ArrayList<String> valueList = new ArrayList<>();
            valueList.add( karte03.getName() );
            searchDef.addName( valueList, CardSearchOperator.IN );
            findByList = repo.find( searchDef );
        }
        catch ( RepositoryException ex )
        {
            fail( "RepositoryEx: " + msgTeilString );
        }
        catch ( CardSearchDefinitionException e )
        {
            fail( "CardSearchEx: " + msgTeilString );
        }
        assertNotNull( msgTeilString, findByList );
        assertEquals( msgTeilString + " Size", 1, findByList.size() );

        // (20) FindByName Operator "IN"
        findByList = null;
        msgTeilString = "findByName IN (" + karte03.getName() + " , " + karte04.getName() + ")";
        try
        {
            searchDef = new CardSearchDefinition();
            ArrayList<String> valueList = new ArrayList<>();
            valueList.add( karte03.getName() );
            valueList.add( karte04.getName() );
            searchDef.addName( valueList, CardSearchOperator.IN );
            findByList = repo.find( searchDef );
        }
        catch ( RepositoryException ex )
        {
            fail( "RepositoryEx: " + msgTeilString );
        }
        catch ( CardSearchDefinitionException e )
        {
            fail( "CardSearchEx: " + msgTeilString );
        }
        assertNotNull( msgTeilString, findByList );
        assertEquals( msgTeilString + " Size", 2, findByList.size() );

        // (21) FindByName Operator "IN"
        findByList = null;
        msgTeilString =
            "findByName IN (" + karte03.getName() + " , " + karte04.getName() + " , " + karte02.getName() + ")";
        try
        {
            searchDef = new CardSearchDefinition();
            ArrayList<String> valueList = new ArrayList<>();
            valueList.add( karte03.getName() );
            valueList.add( karte04.getName() );
            valueList.add( karte02.getName() );
            searchDef.addName( valueList, CardSearchOperator.IN );
            findByList = repo.find( searchDef );
        }
        catch ( RepositoryException ex )
        {
            fail( "RepositoryEx: " + msgTeilString );
        }
        catch ( CardSearchDefinitionException e )
        {
            fail( "CardSearchEx: " + msgTeilString );
        }
        assertNotNull( msgTeilString, findByList );
        assertEquals( msgTeilString + " Size", 4, findByList.size() );

        // (22) FindByName Operator "IN"
        findByList = null;
        msgTeilString = "findByName IN ( keine Wert )";
        try
        {
            searchDef = new CardSearchDefinition();
            ArrayList<String> valueList = new ArrayList<>();
            searchDef.addName( valueList, CardSearchOperator.IN );
            findByList = repo.find( searchDef );
        }
        catch ( RepositoryException ex )
        {
            assertEquals( msgTeilString + " Exception", RepositoryException.CARD_SEARCH_EXCEPTION, ex.getType() );
        }
        catch ( CardSearchDefinitionException e )
        {
            fail( "CardSearchEx: " + msgTeilString );
        }
        assertNull( msgTeilString, findByList );
    }
}

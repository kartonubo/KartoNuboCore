package de.kartonubo.core.json;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.kartonubo.core.data.Card;
import de.kartonubo.core.exception.JsonParseException;
import de.kartonubo.core.exception.ValueValidateException;

import static org.junit.Assert.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CardJsonTest
{
    private static final Logger logger = LoggerFactory.getLogger( CardJsonTest.class );

    @Test
    public void encodeDecodeJsonString()
    {
        String[] tags = { "test", "hallo" };
        Card testCard = createCard( "unitTest", "de.kartonubo.test", "TestName",
                        "df ajf adsjfdkf sdlkjf \nldsjflsdjf sda", null, tags );
        assertNotNull( testCard );

        CardJson service = new CardJson();

        String jsonStr = service.encode( testCard );
        assertNotNull( jsonStr );
        logger.info( "Einzeilige Ausgabe:\n" + jsonStr );
        assertEquals( 1, countLines( jsonStr ) );
        executeDecodeTest( service, jsonStr, testCard );

        service.setPrettyPrint( true );
        jsonStr = service.encode( testCard );
        assertNotNull( jsonStr );
        logger.info( "PrettyPrint Ausgabe:\n" + jsonStr );
        assertEquals( 13, countLines( jsonStr ) );
        executeDecodeTest( service, jsonStr, testCard );

        service.setHumanJson( true );
        jsonStr = service.encode( testCard );
        assertNotNull( jsonStr );
        logger.trace( "HumanJson Ausgabe:\n" + jsonStr );
        assertEquals( 18, countLines( jsonStr ) );
        executeDecodeTest( service, jsonStr, testCard );
    }

    //
    // Private Methods
    //

    private void executeDecodeTest( CardJson service, String jsonStr, Card comparisonCard )
    {
        try
        {
            Card ergCard = service.decode( jsonStr );
            checkCard( ergCard, comparisonCard );
        }
        catch ( JsonParseException e )
        {
            fail("DecodeError");
        }
    }
    
    private void checkCard( Card cardToCheck, Card comparisonCard )
    {
        assertNotNull(cardToCheck);
        assertEquals("ArtefacId", comparisonCard.getArtifactId(), cardToCheck.getArtifactId() );
        assertEquals("GroupId", comparisonCard.getGroupId(), cardToCheck.getGroupId() );
        assertEquals("Name", comparisonCard.getName(), cardToCheck.getName() );
        assertEquals("Text", comparisonCard.getText(), cardToCheck.getText() );
        assertEquals("Comment", comparisonCard.getComment(), cardToCheck.getComment() );
        assertEquals("Tags", comparisonCard.getTagList(), cardToCheck.getTagList() );
    }

    private Card createCard( String artefactId, String groupId, String name, String text, String comment,
                                    String[] tags )
    {
        Card newCard = null;
        try
        {
            newCard = new Card( groupId, artefactId );
            newCard.setName( name );
            newCard.setText( text );
            newCard.setComment( comment );
            for ( int i = 0; i < tags.length; i++ )
            {
                newCard.addTag( tags[i] );
            }
        }
        catch ( ValueValidateException e )
        {
            fail("Create Card");
        }

        return newCard;
    }

    private int countLines( String str )
    {
        Matcher m = Pattern.compile( "\r\n|\r|\n" ).matcher( str );
        int lines = 1;
        while ( m.find() )
        {
            lines++;
        }
        return lines;
    }

}

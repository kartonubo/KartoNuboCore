/*
 *  Copyright 2016 Thomas Heynk
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package de.kartonubo.core.repository;

import de.kartonubo.core.exception.CardSearchDefinitionException;
import de.kartonubo.core.exception.OrientDBManagerException;
import de.kartonubo.core.exception.RepositoryException;
import de.kartonubo.core.exception.ValueValidateException;
import de.kartonubo.core.search.CardSearchDefinition;

import com.orientechnologies.orient.core.command.OCommandRequest;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.sql.OCommandSQL;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.impls.orient.OrientBaseGraph;
import com.tinkerpop.blueprints.impls.orient.OrientGraphNoTx;
import com.tinkerpop.blueprints.impls.orient.OrientVertex;
import com.tinkerpop.blueprints.impls.orient.OrientVertexType;

import de.kartonubo.core.data.Card;
import de.kartonubo.core.service.OrientDBManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Thomas
 */
public class CardRepository
{
    private static final Logger logger = LoggerFactory.getLogger( CardRepository.class );

    private static final String CARD_MIT_DER_ID_GIBT_ES_MEHRMALS = "Card mit der Id: {}  gibt es mehrmals!";

    private static final String ABBRUCH_UNGUELTIGER_SUCH_PARAMETER = "Abbruch wegen ungueltigen Suchparametern!";

    public static final String ATTR_TYPE_NAME = "Card";

    public static final String ATTR_CLASS_NAME = "class:Card";

    public static final String ATTR_CARD_ID = "cardId";

    public static final String ATTR_GROUP_ID = "groupId";

    public static final String ATTR_ARTIFACT_ID = "artifactId";

    public static final String ATTR_NAME = "name";

    public static final String ATTR_COMMENT = "comment";

    public static final String ATTR_TEXT = "text";

    public static final String ATTR_TAG_LIST = "tagList";

    public static final String ATTR_LAST_UPDATE = "lastUpdate";

    public static final String INDEX_UNIQUE = "UNIQUE";

    public static final String INDEX_NOTUNIQUE = "NOTUNIQUE";

    private final OrientDBManager dbManager;

    private OrientBaseGraph baseGraph;

    private boolean autoShutdownEnable;

    private int resultLimit = 100;

    /**
     * @throws RepositoryException
     * @throws OrientDBManagerException
     */
    public CardRepository() throws RepositoryException
    {
        dbManager = new OrientDBManager();
        try
        {
            dbManager.openFactory();
            createCardClassIfNecessary();
            createCardPropertiesIfNecessary();
            createCardIndexIfNecessary();
        }
        catch ( OrientDBManagerException e )
        {
            throw new RepositoryException( "Fehler bei Initialisierung von CardRepository!",
                                           RepositoryException.DB_MANAGER_EXCEPTION, e );
        }
        autoShutdownEnable = true;
    }

    /**
     *
     */
    public void closeAll()
    {
        logger.debug( "closeAll!!!!" );
        shutdownGraph();
        dbManager.closeFactory();
    }

    /**
     * @param value
     */
    public void setAutoShutdownEnable( boolean value )
    {
        autoShutdownEnable = value;
    }

    /**
     * @return
     */
    public boolean isAutoShutdownEnable()
    {
        return autoShutdownEnable;
    }

    /**
     *
     */
    public void shutdownGraph()
    {
        logger.trace( "shutdownGraph called!" );
        if ( !isAutoShutdownEnable() )
        {
            return;
        }
        if ( baseGraph != null )
        {
            baseGraph.shutdown();
            baseGraph = null;
        }
    }

    /**
     * @return
     */
    public int getResultLimit()
    {
        return resultLimit;
    }

    /**
     * @param resultLimit
     */
    public void setResultLimit( int resultLimit )
    {
        this.resultLimit = resultLimit;
    }

    /**
     * @param groupId
     * @param artifactId
     * @return
     * @throws RepositoryException
     */
    public Card createCard( String groupId, String artifactId ) throws RepositoryException
    {
        logger.trace( "Beginn Methode createCard" );

        String cardId = createCardID( groupId, artifactId );

        if ( existObjectWithCardId( cardId ) )
        {
            throw new RepositoryException( "Card mit der ID '" + cardId
                + "' existiert, und kann nicht erstellt werden.", RepositoryException.CARD_EXISTS );
        }

        try
        {
            Vertex vCard = getGraph().addVertex( ATTR_CLASS_NAME );
            vCard.setProperty( ATTR_CARD_ID, cardId );
            vCard.setProperty( ATTR_GROUP_ID, groupId );
            vCard.setProperty( ATTR_ARTIFACT_ID, artifactId );
            Date lastUpdate = new Date();
            vCard.setProperty( ATTR_LAST_UPDATE, lastUpdate );
            getGraph().commit();

            Card ergebnis = new Card( groupId, artifactId );
            ergebnis.setLastUpdate( lastUpdate );
            return ergebnis;
        }
        catch ( Exception e )
        {
            getGraph().rollback();
            throw new RepositoryException( "Rollback in Methode createCard(groupId,aritfactId)",
                                           RepositoryException.DB_EXCEPTION, e );
        }
        finally
        {
            shutdownGraph();
        }
    }

    /**
     * @param groupId
     * @param artifactId
     * @return
     * @throws RepositoryException
     */
    public Card getCard( String groupId, String artifactId ) throws RepositoryException
    {
        logger.trace( "Beginn Methode getCard" );

        String cardId = createCardID( groupId, artifactId );

        try
        {
            Iterable<Vertex> resultset = getGraph().getVertices( ATTR_CARD_ID, cardId );
            if ( resultset != null && size( resultset ) > 0 )
            {
                if ( size( resultset ) > 1 )
                {
                    logger.warn( CARD_MIT_DER_ID_GIBT_ES_MEHRMALS, cardId );
                }
                Iterator<Vertex> iter = resultset.iterator();
                Vertex vCard = iter.next();
                return convertVertexToCard( vCard );
            }
        }
        catch ( Exception e )
        {
            getGraph().rollback();
            throw new RepositoryException( "Rollback in Methode getCard(groupId,aritfactId)",
                                           RepositoryException.DB_EXCEPTION, e );
        }
        finally
        {
            shutdownGraph();
        }
        throw new RepositoryException( "Card mit der Id '" + cardId + "' existiert nicht!",
                                       RepositoryException.CARD_NOT_EXISTS );
    }

    /**
     * @param groupId
     * @param artifactId
     * @throws RepositoryException
     */
    public void deleteCard( String groupId, String artifactId ) throws RepositoryException
    {
        logger.trace( "Beginn Methode deleteCard" );

        String cardId = createCardID( groupId, artifactId );

        try
        {
            Iterable<Vertex> resultset = getGraph().getVertices( ATTR_CARD_ID, cardId );
            if ( resultset != null && size( resultset ) > 0 )
            {
                if ( size( resultset ) > 1 )
                {
                    logger.warn( CARD_MIT_DER_ID_GIBT_ES_MEHRMALS, cardId );
                }
                Iterator<Vertex> iter = resultset.iterator();
                Vertex vCard;
                while ( iter.hasNext() )
                {
                    vCard = iter.next();
                    vCard.remove();
                }
            }
        }
        catch ( Exception e )
        {
            getGraph().rollback();
            throw new RepositoryException( "Rollback in Methode deleteCard(groupId,aritfactId)",
                                           RepositoryException.DB_EXCEPTION, e );
        }
        finally
        {
            shutdownGraph();
        }
    }

    /**
     * @param obj
     * @return
     * @throws RepositoryException
     */
    public Card createCard( Card obj ) throws RepositoryException
    {
        logger.trace( "Beginn Methode createCard(obj)" );

        String cardId = createCardID( obj );

        try
        {
            if ( existObjectWithCardId( cardId ) )
            {
                throw new RepositoryException( "Card mit der ID '" + cardId
                    + "' existiert, und kann nicht erstellt werden.", RepositoryException.CARD_EXISTS );
            }
            Vertex vCard = getGraph().addVertex( ATTR_CLASS_NAME );
            vCard.setProperty( ATTR_CARD_ID, cardId );
            vCard.setProperty( ATTR_GROUP_ID, obj.getGroupId() );
            vCard.setProperty( ATTR_ARTIFACT_ID, obj.getArtifactId() );

            Card ergebnis = new Card( obj.getGroupId(), obj.getArtifactId() );

            vCard.setProperty( ATTR_NAME, obj.getName() );
            ergebnis.setName( obj.getName() );
            vCard.setProperty( ATTR_COMMENT, obj.getComment() );
            ergebnis.setComment( obj.getComment() );
            vCard.setProperty( ATTR_TEXT, obj.getText() );
            ergebnis.setText( obj.getText() );
            vCard.setProperty( ATTR_TAG_LIST, obj.getTagList() );
            ergebnis.setTagList( obj.getTagList() );

            Date lastUpdate = new Date();
            vCard.setProperty( ATTR_LAST_UPDATE, lastUpdate );
            getGraph().commit();

            ergebnis.setLastUpdate( lastUpdate );
            return ergebnis;
        }
        catch ( Exception e )
        {
            logger.error( "Rollback in Methode createCard(obj)", e );
            getGraph().rollback();
            throw new RepositoryException( "Rollback in Methode createCard(obj)", RepositoryException.DB_EXCEPTION, e );
        }
        finally
        {
            shutdownGraph();
        }
    }

    /**
     * @param obj
     * @return
     */
    public Card updateCard( Card obj ) throws RepositoryException
    {
        logger.trace( "Beginn Methode updateCard(obj)" );

        String cardId = createCardID( obj );

        try
        {
            Iterable<Vertex> resultset = getGraph().getVertices( ATTR_CARD_ID, cardId );
            if ( resultset != null && size( resultset ) > 0 )
            {
                if ( size( resultset ) > 1 )
                {
                    logger.error( CARD_MIT_DER_ID_GIBT_ES_MEHRMALS, cardId );
                    throw new RepositoryException( "Abbruch updateCard(obj)", RepositoryException.MULTIPLE_CARDS );
                }
                Iterator<Vertex> iter = resultset.iterator();
                Vertex vCard = iter.next();

                Card ergebnis = new Card( obj.getGroupId(), obj.getArtifactId() );

                vCard.setProperty( ATTR_NAME, obj.getName() );
                ergebnis.setName( obj.getName() );
                vCard.setProperty( ATTR_COMMENT, obj.getComment() );
                ergebnis.setComment( obj.getComment() );
                vCard.setProperty( ATTR_TEXT, obj.getText() );
                ergebnis.setText( obj.getText() );
                vCard.setProperty( ATTR_TAG_LIST, obj.getTagList() );
                ergebnis.setTagList( obj.getTagList() );

                Date lastUpdate = new Date();
                vCard.setProperty( ATTR_LAST_UPDATE, lastUpdate );
                getGraph().commit();

                ergebnis.setLastUpdate( lastUpdate );
                return ergebnis;
            }
        }
        catch ( Exception e )
        {
            logger.error( "Rollback in Methode updateCard(obj)", e );
            getGraph().rollback();
            throw new RepositoryException( "Rollback in Methode updateCard(obj)", RepositoryException.DB_EXCEPTION, e );
        }
        finally
        {
            shutdownGraph();
        }
        throw new RepositoryException( "Card mit der Id '" + cardId + "' existiert nicht!",
                                       RepositoryException.CARD_NOT_EXISTS );
    }

    /**
     * @param obj
     * @throws de.kartonubo.core.exception.RepositoryException
     */
    public void deleteCard( Card obj ) throws RepositoryException
    {
        if ( obj == null )
        {
            throw new RepositoryException( "Card nicht DEFINIERT!", RepositoryException.CARD_NOT_DEFINED );
        }
        deleteCard( obj.getGroupId(), obj.getArtifactId() );
    }

    /**
     * @param artifactId
     * @return
     * @throws RepositoryException
     * @throws CardSearchDefinitionException
     */
    public List<Card> findByArtifactId( String artifactId ) throws RepositoryException
    {
        CardSearchDefinition searchDef = new CardSearchDefinition();
        try
        {
            searchDef.addArtifactId( artifactId );
        }
        catch ( CardSearchDefinitionException e )
        {
            throw new RepositoryException( ABBRUCH_UNGUELTIGER_SUCH_PARAMETER,
                                           RepositoryException.CARD_SEARCH_EXCEPTION, e );
        }
        return find( searchDef );
    }

    /**
     * @param groupId
     * @return
     * @throws RepositoryException
     */
    public List<Card> findByGroupId( String groupId ) throws RepositoryException
    {
        CardSearchDefinition searchDef = new CardSearchDefinition();
        try
        {
            searchDef.addGroupId( groupId );
        }
        catch ( CardSearchDefinitionException e )
        {
            throw new RepositoryException( ABBRUCH_UNGUELTIGER_SUCH_PARAMETER,
                                           RepositoryException.CARD_SEARCH_EXCEPTION, e );
        }
        return find( searchDef );
    }

    /**
     * @param name
     * @return
     * @throws RepositoryException
     */
    public List<Card> findByName( String name ) throws RepositoryException
    {
        CardSearchDefinition searchDef = new CardSearchDefinition();
        try
        {
            searchDef.addName( name );
        }
        catch ( CardSearchDefinitionException e )
        {
            throw new RepositoryException( ABBRUCH_UNGUELTIGER_SUCH_PARAMETER,
                                           RepositoryException.CARD_SEARCH_EXCEPTION, e );
        }
        return find( searchDef );
    }

    /**
     * @param searchDef
     * @return
     * @throws RepositoryException
     * @throws CardSearchDefinitionException
     */
    public List<Card> find( CardSearchDefinition searchDef ) throws RepositoryException
    {
        final List<String> list = new ArrayList<>();
        String sqlStr;
        try
        {
            list.addAll( searchDef.getPreparedValues() );
            sqlStr = searchDef.getPreparedStatement();
        }
        catch ( CardSearchDefinitionException e )
        {
            throw new RepositoryException( ABBRUCH_UNGUELTIGER_SUCH_PARAMETER,
                                           RepositoryException.CARD_SEARCH_EXCEPTION, e );
        }

        List<OrientVertex> resultList = command( "sql", sqlStr, list.toArray(), getResultLimit() );

        return convertVertexListToCardList( resultList );
    }

    //
    // Private Methods
    //

    private void createCardClassIfNecessary() throws OrientDBManagerException
    {
        OrientGraphNoTx noTxGraph = dbManager.createGraphWithNoTx();
        try
        {
            if ( noTxGraph.getVertexType( ATTR_TYPE_NAME ) == null )
            {
                noTxGraph.createVertexType( ATTR_TYPE_NAME );
            }
        }
        finally
        {
            noTxGraph.shutdown();
        }
    }

    private void createCardPropertiesIfNecessary() throws RepositoryException
    {
        OrientGraphNoTx noTxGraph = createGrapWithNoTx();
        OrientVertexType cardVertexType = getCardVertexType( noTxGraph );
        try
        {
            createCardProperty( cardVertexType, ATTR_CARD_ID, OType.STRING );
            createCardProperty( cardVertexType, ATTR_GROUP_ID, OType.STRING );
            createCardProperty( cardVertexType, ATTR_ARTIFACT_ID, OType.STRING );
            createCardProperty( cardVertexType, ATTR_NAME, OType.STRING );
            createCardProperty( cardVertexType, ATTR_COMMENT, OType.STRING );
            createCardProperty( cardVertexType, ATTR_TEXT, OType.STRING );
            createCardProperty( cardVertexType, ATTR_TAG_LIST, OType.STRING );
            createCardProperty( cardVertexType, ATTR_LAST_UPDATE, OType.DATE );
        }
        finally
        {
            noTxGraph.shutdown();
        }
    }

    private void createCardProperty( OrientVertexType vertexType, String propertyName, OType propertyType )
    {
        if ( vertexType.getProperty( propertyName ) == null )
        {
            vertexType.createProperty( propertyName, propertyType );
        }
    }

    private void createCardIndexIfNecessary() throws RepositoryException
    {

        OrientGraphNoTx noTxGraph = createGrapWithNoTx();
        OrientVertexType cardVertexType = getCardVertexType( noTxGraph );
        try
        {
            createCardIndex( cardVertexType, ATTR_CARD_ID, INDEX_UNIQUE );
            createCardIndex( cardVertexType, ATTR_GROUP_ID, INDEX_NOTUNIQUE );
            createCardIndex( cardVertexType, ATTR_ARTIFACT_ID, INDEX_NOTUNIQUE );
            createCardIndex( cardVertexType, ATTR_NAME, INDEX_NOTUNIQUE );
            createCardFullTextIndex( cardVertexType, ATTR_COMMENT );
            createCardFullTextIndex( cardVertexType, ATTR_COMMENT );
        }
        finally
        {
            noTxGraph.shutdown();
        }
    }

    private void createCardIndex( OrientVertexType vertexType, String indexNameAndField, String indexAlgorithm )
    {
        if ( vertexType.getClassIndex( indexNameAndField ) != null )
        {
            vertexType.createIndex( indexNameAndField, indexAlgorithm, indexNameAndField );
        }
    }

    private void createCardFullTextIndex( OrientVertexType vertexType, String indexNameAndField )
    {
        if ( vertexType.getClassIndex( indexNameAndField ) != null )
        {
            ODocument metadata = new ODocument();
            metadata.field( "indexRadix", true );
            // Quelle:
            // http://staticfloat.com/allgemein/deutsche-stoppwort-liste-german-stopwords/
            metadata.field( "stopWords", Arrays.asList( new String[] { "ab", "bei", "da", "deshalb", "ein", "für",
                "finde", "haben", "hier", "ich", "ja", "kann", "machen", "muesste", "nach", "oder", "seid", "sonst",
                "und", "vom", "wann", "wenn", "wie", "zu", "bin", "eines", "hat", "manche", "solches", "an", "anderm",
                "bis", "das", "deinem", "demselben", "dir", "doch", "einig", "er", "eurer", "hatte", "ihnen", "ihre",
                "ins", "jenen", "keinen", "manchem", "meinen", "nichts", "seine", "soll", "unserm", "welche", "werden",
                "wollte", "während", "alle", "allem", "allen", "aller", "alles", "als", "also", "am", "ander", "andere",
                "anderem", "anderen", "anderer", "anderes", "andern", "anders", "auch", "auf", "aus", "bist", "bsp.",
                "daher", "damit", "dann", "dasselbe", "dazu", "daß", "dein", "deine", "deinen", "deiner", "deines",
                "dem", "den", "denn", "denselben", "der", "derer", "derselbe", "derselben", "des", "desselben",
                "dessen", "dich", "die", "dies", "diese", "dieselbe", "dieselben", "diesem", "diesen", "dieser",
                "dieses", "dort", "du", "durch", "eine", "einem", "einen", "einer", "einige", "einigem", "einigen",
                "einiger", "einiges", "einmal", "es", "etwas", "euch", "euer", "eure", "eurem", "euren", "eures",
                "ganz", "ganze", "ganzen", "ganzer", "ganzes", "gegen", "gemacht", "gesagt", "gesehen", "gewesen",
                "gewollt", "hab", "habe", "hatten", "hin", "hinter", "ihm", "ihn", "ihr", "ihrem", "ihren", "ihrer",
                "ihres", "im", "in", "indem", "ist", "jede", "jedem", "jeden", "jeder", "jedes", "jene", "jenem",
                "jener", "jenes", "jetzt", "kein", "keine", "keinem", "keiner", "keines", "konnte", "könnten", "können",
                "könnte", "mache", "machst", "macht", "machte", "machten", "man", "manchen", "mancher", "manches",
                "mein", "meine", "meinem", "meiner", "meines", "mich", "mir", "mit", "muss", "musste", "müßt", "nicht",
                "noch", "nun", "nur", "ob", "ohne", "sage", "sagen", "sagt", "sagte", "sagten", "sagtest", "sehe",
                "sehen", "sehr", "seht", "sein", "seinem", "seinen", "seiner", "seines", "selbst", "sich", "sicher",
                "sie", "sind", "so", "solche", "solchem", "solchen", "solcher", "sollte", "sondern", "um", "uns",
                "unse", "unsen", "unser", "unses", "unter", "viel", "von", "vor", "war", "waren", "warst", "was", "weg",
                "weil", "weiter", "welchem", "welchen", "welcher", "welches", "welche", "werde", "wieder", "will",
                "wir", "wird", "wirst", "wo", "wolle", "wollen", "wollt", "wollten", "wolltest", "wolltet", "würde",
                "würden", "z.B.", "zum", "zur", "zwar", "zwischen", "über", "aber", "abgerufen", "abgerufene",
                "abgerufener", "abgerufenes", "acht", "allein", "allerdings", "allerlei", "allgemein", "allmählich",
                "allzu", "alsbald", "andererseits", "andernfalls", "anerkannt", "anerkannte", "anerkannter",
                "anerkanntes", "anfangen", "anfing", "angefangen", "angesetze", "angesetzt", "angesetzten",
                "angesetzter", "ansetzen", "anstatt", "arbeiten", "aufgehört", "aufgrund", "aufhören", "aufhörte",
                "aufzusuchen", "ausdrücken", "ausdrückt", "ausdrückte", "ausgenommen", "ausser", "ausserdem", "author",
                "autor", "außen", "außer", "außerdem", "außerhalb", "bald", "bearbeite", "bearbeiten", "bearbeitete",
                "bearbeiteten", "bedarf", "bedurfte", "bedürfen", "befragen", "befragte", "befragten", "befragter",
                "begann", "beginnen", "begonnen", "behalten", "behielt", "beide", "beiden", "beiderlei", "beides",
                "beim", "bei", "beinahe", "beitragen", "beitrugen", "bekannt", "bekannte", "bekannter", "bekennen",
                "benutzt", "bereits", "berichten", "berichtet", "berichtete", "berichteten", "besonders", "besser",
                "bestehen", "besteht", "beträchtlich", "bevor", "bezüglich", "bietet", "bisher", "bislang", "bis",
                "bleiben", "blieb", "bloss", "bloß", "brachte", "brachten", "brauchen", "braucht", "bringen",
                "bräuchte", "bzw", "böden", "ca.", "dabei", "dadurch", "dafür", "dagegen", "dahin", "damals", "danach",
                "daneben", "dank", "danke", "danken", "dannen", "daran", "darauf", "daraus", "darf", "darfst", "darin",
                "darum", "darunter", "darüber", "darüberhinaus", "dass", "davon", "davor", "demnach", "denen",
                "dennoch", "derart", "derartig", "derem", "deren", "derjenige", "derjenigen", "derzeit", "desto",
                "deswegen", "diejenige", "diesseits", "dinge", "direkt", "direkte", "direkten", "direkter", "doppelt",
                "dorther", "dorthin", "drauf", "drei", "dreißig", "drin", "dritte", "drunter", "drüber", "dunklen",
                "durchaus", "durfte", "durften", "dürfen", "dürfte", "eben", "ebenfalls", "ebenso", "ehe", "eher",
                "eigenen", "eigenes", "eigentlich", "einbaün", "einerseits", "einfach", "einführen", "einführte",
                "einführten", "eingesetzt", "einigermaßen", "eins", "einseitig", "einseitige", "einseitigen",
                "einseitiger", "einst", "einstmals", "einzig", "ende", "entsprechend", "entweder", "ergänze",
                "ergänzen", "ergänzte", "ergänzten", "erhalten", "erhielt", "erhielten", "erhält", "erneut", "erst",
                "erste", "ersten", "erster", "eröffne", "eröffnen", "eröffnet", "eröffnete", "eröffnetes", "etc",
                "etliche", "etwa", "fall", "falls", "fand", "fast", "ferner", "finden", "findest", "findet", "folgende",
                "folgenden", "folgender", "folgendes", "folglich", "fordern", "fordert", "forderte", "forderten",
                "fortsetzen", "fortsetzt", "fortsetzte", "fortsetzten", "fragte", "frau", "frei", "freie", "freier",
                "freies", "fuer", "fünf", "gab", "ganzem", "gar", "gbr", "geb", "geben", "geblieben", "gebracht",
                "gedurft", "geehrt", "geehrte", "geehrten", "geehrter", "gefallen", "gefiel", "gefälligst", "gefällt",
                "gegeben", "gehabt", "gehen", "geht", "gekommen", "gekonnt", "gemocht", "gemäss", "genommen", "genug",
                "gern", "gestern", "gestrige", "getan", "geteilt", "geteilte", "getragen", "gewissermaßen", "geworden",
                "ggf", "gib", "gibt", "gleich", "gleichwohl", "gleichzeitig", "glücklicherweise", "gmbh", "gratulieren",
                "gratuliert", "gratulierte", "gut", "gute", "guten", "gängig", "gängige", "gängigen", "gängiger",
                "gängiges", "gänzlich", "haette", "halb", "hallo", "hast", "hattest", "hattet", "heraus", "herein",
                "heute", "heutige", "hiermit", "hiesige", "hinein", "hinten", "hinterher", "hoch", "hundert", "hätt",
                "hätte", "hätten", "höchstens", "igitt", "immer", "immerhin", "important", "indessen", "info",
                "infolge", "innen", "innerhalb", "insofern", "inzwischen", "irgend", "irgendeine", "irgendwas",
                "irgendwen", "irgendwer", "irgendwie", "irgendwo", "je", "jedenfalls", "jederlei", "jedoch", "jemand",
                "jenseits", "jährig", "jährige", "jährigen", "jähriges", "kam", "kannst", "kaum", "keines", "keinerlei",
                "keineswegs", "klar", "klare", "klaren", "klares", "klein", "kleinen", "kleiner", "kleines", "koennen",
                "koennt", "koennte", "koennten", "komme", "kommen", "kommt", "konkret", "konkrete", "konkreten",
                "konkreter", "konkretes", "konnten", "könn", "könnt", "könnten", "künftig", "lag", "lagen", "langsam",
                "lassen", "laut", "lediglich", "leer", "legen", "legte", "legten", "leicht", "leider", "lesen", "letze",
                "letzten", "letztendlich", "letztens", "letztes", "letztlich", "lichten", "liegt", "liest", "links",
                "längst", "längstens", "mag", "magst", "mal", "mancherorts", "manchmal", "mann", "margin", "mehr",
                "mehrere", "meist", "meiste", "meisten", "meta", "mindestens", "mithin", "mochte", "morgen", "morgige",
                "muessen", "muesst", "musst", "mussten", "muß", "mußt", "möchte", "möchten", "möchtest", "mögen",
                "möglich", "mögliche", "möglichen", "möglicher", "möglicherweise", "müssen", "müsste", "müssten",
                "müßte", "nachdem", "nacher", "nachhinein", "nahm", "natürlich", "nacht", "neben", "nebenan", "nehmen",
                "nein", "neu", "neue", "neuem", "neuen", "neuer", "neues", "neun", "nie", "niemals", "niemand", "nimm",
                "nimmer", "nimmt", "nirgends", "nirgendwo", "nutzen", "nutzt", "nutzung", "nächste", "nämlich",
                "nötigenfalls", "nützt", "oben", "oberhalb", "obgleich", "obschon", "obwohl", "oft", "per", "pfui",
                "plötzlich", "pro", "reagiere", "reagieren", "reagiert", "reagierte", "rechts", "regelmäßig", "rief",
                "rund", "sang", "sangen", "schlechter", "schließlich", "schnell", "schon", "schreibe", "schreiben",
                "schreibens", "schreiber", "schwierig", "schätzen", "schätzt", "schätzte", "schätzten", "sechs", "sect",
                "sehrwohl", "sei", "seit", "seitdem", "seite", "seiten", "seither", "selber", "senke", "senken",
                "senkt", "senkte", "senkten", "setzen", "setzt", "setzte", "setzten", "sicherlich", "sieben", "siebte",
                "siehe", "sieht", "singen", "singt", "sobald", "sodaß", "soeben", "sofern", "sofort", "sog", "sogar",
                "solange", "solc", "hen", "solch", "sollen", "sollst", "sollt", "sollten", "solltest", "somit",
                "sonstwo", "sooft", "soviel", "soweit", "sowie", "sowohl", "spielen", "später", "startet", "startete",
                "starteten", "statt", "stattdessen", "steht", "steige", "steigen", "steigt", "stets", "stieg",
                "stiegen", "such", "suchen", "sämtliche", "tages", "tat", "tatsächlich", "tatsächlichen",
                "tatsächlicher", "tatsächliches", "tausend", "teile", "teilen", "teilte", "teilten", "titel", "total",
                "trage", "tragen", "trotzdem", "trug", "trägt", "toll", "tun", "tust", "tut", "txt", "tät", "ueber",
                "umso", "unbedingt", "ungefähr", "unmöglich", "unmögliche", "unmöglichen", "unmöglicher", "unnötig",
                "unsem", "unser", "unsere", "unserem", "unseren", "unserer", "unseres", "unten", "unterbrach",
                "unterbrechen", "unterhalb", "unwichtig", "usw", "vergangen", "vergangene", "vergangener",
                "vergangenes", "vermag", "vermutlich", "vermögen", "verrate", "verraten", "verriet", "verrieten",
                "version", "versorge", "versorgen", "versorgt", "versorgte", "versorgten", "versorgtes",
                "veröffentlichen", "veröffentlicher", "veröffentlicht", "veröffentlichte", "veröffentlichten",
                "veröffentlichtes", "viele", "vielen", "vieler", "vieles", "vielleicht", "vielmals", "vier",
                "vollständig", "voran", "vorbei", "vorgestern", "vorher", "vorne", "vorüber", "völlig", "während",
                "wachen", "waere", "warum", "weder", "wegen", "weitere", "weiterem", "weiteren", "weiterer", "weiteres",
                "weiterhin", "weiß", "wem", "wen", "wenig", "wenige", "weniger", "wenigstens", "wenngleich", "wer",
                "werdet", "weshalb", "wessen", "weswegen", "wichtig", "wieso", "wieviel", "wiewohl", "willst",
                "wirklich", "wodurch", "wogegen", "woher", "wohin", "wohingegen", "wohl", "wohlweislich", "womit",
                "woraufhin", "woraus", "worin", "wurde", "wurden", "währenddessen", "wär", "wäre", "wären", "zahlreich",
                "zehn", "zeitweise", "ziehen", "zieht", "zog", "zogen", "zudem", "zuerst", "zufolge", "zugleich",
                "zuletzt", "zumal", "zurück", "zusammen", "zuviel", "zwanzig", "zwei", "zwölf", "ähnlich", "übel",
                "überall", "überallhin", "überdies", "übermorgen", "übrig", "übrigens" } ) );
            metadata.field( "separatorChars", " ,.:;?[]()" );
            // metadata.field("ignoreChars", "$&"); //NOSONAR
            metadata.field( "minWordLength", 5 );

            vertexType.createIndex( indexNameAndField, "FULLTEXT", null, metadata, null,
                            new String[] { indexNameAndField } );
        }
    }

    private OrientGraphNoTx createGrapWithNoTx() throws RepositoryException
    {
        OrientGraphNoTx noTxGraph;
        try
        {
            noTxGraph = dbManager.createGraphWithNoTx();
        }
        catch ( OrientDBManagerException odbEx )
        {
            String message = "OrientDBManagerException vom Typ " + odbEx.getType() + ": " + odbEx.getMessage();
            throw new RepositoryException( message, RepositoryException.ORIENT_DB_MANAGER_EX, odbEx );
        }
        return noTxGraph;
    }

    private OrientVertexType getCardVertexType( OrientGraphNoTx noTxGraph ) throws RepositoryException
    {

        OrientVertexType cardVertexType = noTxGraph.getVertexType( ATTR_TYPE_NAME );
        if ( cardVertexType == null )
        {
            throw new RepositoryException( "VertexType " + ATTR_TYPE_NAME + " nicht DEFINIERT!",
                                           RepositoryException.TYP_CARD_NOT_DEFINED );
        }
        return cardVertexType;
    }

    // Überprüft die Korrektheit der Parameter und erstellt die CardID.
    private String createCardID( Card obj ) throws RepositoryException
    {
        if ( obj == null )
        {
            throw new RepositoryException( "Card nicht DEFINIERT!", RepositoryException.CARD_NOT_DEFINED );
        }
        return createCardID( obj.getGroupId(), obj.getArtifactId() );
    }

    // Überprüft die Korrektheit der Parameter und erstellt die CardID.
    private String createCardID( String groupId, String artifactId ) throws RepositoryException
    {
        if ( StringUtils.isBlank( groupId ) )
        {
            throw new RepositoryException( "GroupId '" + groupId + "' nicht DEFINIERT!",
                                           RepositoryException.CARDID_NOT_CREATED );
        }
        if ( StringUtils.isBlank( artifactId ) )
        {
            throw new RepositoryException( "ArtifactId '" + artifactId + "' nicht DEFINIERT!",
                                           RepositoryException.CARDID_NOT_CREATED );
        }
        return groupId + "." + artifactId;
    }

    private boolean existObjectWithCardId( String cardId ) throws RepositoryException
    {
        logger.trace( "Beginn Methode existObjectWithCardId" );

        Iterable<Vertex> resultset = getGraph().getVertices( ATTR_CARD_ID, cardId );
        if ( resultset != null && size( resultset ) > 0 )
        {
            logger.debug( "Card mit der Id: {} existiert!", cardId );
            return true;
        }
        return false;
    }

    private OrientBaseGraph getGraph() throws RepositoryException
    {
        logger.trace( "GetGraph called!" );
        if ( baseGraph != null )
        {
            return baseGraph;
        }
        try
        {
            return dbManager.createGraph();
        }
        catch ( OrientDBManagerException e )
        {
            throw new RepositoryException( "OrientBaseGraph konnte nicht erstellt werden!",
                                           RepositoryException.DB_MANAGER_EXCEPTION, e );
        }
    }

    // TODO: Ausgliedern in Utils-Klasse
    private static int size( Iterable<?> data )
    {
        if ( data instanceof Collection )
        {
            return ( (Collection<?>) data ).size();
        }
        int counter = 0;
        for ( Iterator<?> iterator = data.iterator(); iterator.hasNext(); )
        {
            iterator.next();
            counter++;
        }
        return counter;
    }

    private Card convertVertexToCard( Vertex vCard ) throws ValueValidateException
    {
        Card ergebnis =
            new Card( (String) vCard.getProperty( ATTR_GROUP_ID ), (String) vCard.getProperty( ATTR_ARTIFACT_ID ) );
        ergebnis.setName( (String) vCard.getProperty( ATTR_NAME ) );
        ergebnis.setComment( (String) vCard.getProperty( ATTR_COMMENT ) );
        ergebnis.setText( (String) vCard.getProperty( ATTR_TEXT ) );
        ergebnis.setTagList( (String) vCard.getProperty( ATTR_TAG_LIST ) );
        ergebnis.setLastUpdate( (Date) vCard.getProperty( ATTR_LAST_UPDATE ) );
        return ergebnis;
    }

    private List<Card> convertVertexListToCardList( List<OrientVertex> vertexList ) throws RepositoryException
    {
        List<Card> cardList = new ArrayList<>();

        Iterator<OrientVertex> resultSet = vertexList.iterator();
        while ( resultSet.hasNext() )
        {
            OrientVertex vCard = resultSet.next();
            String className = vCard.getRecord().getClassName();
            if ( className.equals( ATTR_TYPE_NAME ) )
            {
                try
                {
                    cardList.add( convertVertexToCard( vCard ) );
                }
                catch ( ValueValidateException e )
                {
                    throw new RepositoryException( "Ungültige Card-Werte in Datenbank.",
                                                   RepositoryException.DB_INVALID_CARD_VALUES, e );
                }
            }
        }
        return cardList;
    }

    @SuppressWarnings( "unchecked" )
    private List<OrientVertex> command( final String language, final String iText, final Object[] iArgs, int limit )
                    throws RepositoryException
    {
        Object result = null;
        if ( "sql".equalsIgnoreCase( language ) )
        {
            OCommandSQL comSql = new OCommandSQL( iText );
            OCommandRequest request = getGraph().command( comSql );
            request.setLimit( limit );
            result = request.execute( iArgs );
        }

        // else if (language.equalsIgnoreCase("gremlin")) { // NOSONAR
        // result = getGraph().command(new OCommandGremlin(iText)).execute(iArgs);
        // } else {
        // result = getGraph().command(new OCommandScript(language,iText)).execute(iArgs);
        // }

        final List<OrientVertex> list = new ArrayList<>();

        if ( result != null && result instanceof Iterable<?> )
        {
            for ( Object obj : (Iterable<Object>) result )
            {
                if ( obj instanceof Vertex )
                {
                    list.add( (OrientVertex) obj );
                }
            }
            return list;
        }
        return list;
    }
}

/*
 *  Copyright 2016 Thomas Heynk
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package de.kartonubo.core.search;

import java.util.ArrayList;
import java.util.List;

import de.kartonubo.core.exception.CardSearchDefinitionException;

/**
 * @author Thomas
 */
public class CardSearchDefinition extends BaseCardSearchDefinition
{

    /**
     * 
     */
    public CardSearchDefinition()
    {
        super();
    }

    //
    // addArtifactId
    //

    /**
     * @param value
     * @throws CardSearchDefinitionException
     */
    public void addArtifactId( String value ) throws CardSearchDefinitionException
    {
        ArrayList<String> valueList = new ArrayList<>();
        valueList.add( value );

        addArtifactId( valueList, value.contains( "%" ) ? CardSearchOperator.LIKE : CardSearchOperator.EQUALS,
                        CardSearchLinkOperator.AND, 0 );
    }

    /**
     * @param valueList
     * @param operator
     * @throws CardSearchDefinitionException
     */
    public void addArtifactId( List<String> valueList, CardSearchOperator operator )
                    throws CardSearchDefinitionException
    {
        addArtifactId( valueList, operator, CardSearchLinkOperator.DEFAULT, 0 );
    }

    /**
     * @param valueList
     * @param operator
     * @param linkOperator
     * @throws CardSearchDefinitionException
     */
    public void addArtifactId( List<String> valueList, CardSearchOperator operator,
                               CardSearchLinkOperator linkOperator ) throws CardSearchDefinitionException
    {
        addArtifactId( valueList, operator, linkOperator, 0 );
    }

    /**
     * @param valueList
     * @param operator
     * @param linkOperator
     * @param seachOrderNr
     * @throws CardSearchDefinitionException
     */
    public void addArtifactId( List<String> valueList, CardSearchOperator operator, CardSearchLinkOperator linkOperator,
                               int seachOrderNr ) throws CardSearchDefinitionException
    {
        addSearchValue( CardSearchAttribut.ARTIFACT_ID, valueList, operator, linkOperator, seachOrderNr );
    }

    //
    // GroupId
    //

    /**
     * @param value
     * @throws CardSearchDefinitionException
     */
    public void addGroupId( String value ) throws CardSearchDefinitionException
    {
        ArrayList<String> valueList = new ArrayList<>();
        valueList.add( value );

        addGroupId( valueList, value.contains( "%" ) ? CardSearchOperator.LIKE : CardSearchOperator.EQUALS,
                        CardSearchLinkOperator.AND, 0 );
    }

    /**
     * @param valueList
     * @param operator
     * @throws CardSearchDefinitionException
     */
    public void addGroupId( List<String> valueList, CardSearchOperator operator ) throws CardSearchDefinitionException
    {
        addGroupId( valueList, operator, CardSearchLinkOperator.DEFAULT, 0 );
    }

    /**
     * @param valueList
     * @param operator
     * @param linkOperator
     * @throws CardSearchDefinitionException
     */
    public void adGourpId( List<String> valueList, CardSearchOperator operator, CardSearchLinkOperator linkOperator )
                    throws CardSearchDefinitionException
    {
        addGroupId( valueList, operator, linkOperator, 0 );
    }

    /**
     * @param valueList
     * @param operator
     * @param linkOperator
     * @param seachOrderNr
     * @throws CardSearchDefinitionException
     */
    public void addGroupId( List<String> valueList, CardSearchOperator operator, CardSearchLinkOperator linkOperator,
                            int seachOrderNr ) throws CardSearchDefinitionException
    {
        addSearchValue( CardSearchAttribut.GROUP_ID, valueList, operator, linkOperator, seachOrderNr );
    }

    //
    // addName
    //

    /**
     * @param value
     * @throws CardSearchDefinitionException
     */
    public void addName( String value ) throws CardSearchDefinitionException
    {
        ArrayList<String> valueList = new ArrayList<>();
        valueList.add( value );

        addName( valueList, value.contains( "%" ) ? CardSearchOperator.LIKE : CardSearchOperator.EQUALS,
                        CardSearchLinkOperator.AND, 0 );
    }

    /**
     * @param valueList
     * @param operator
     * @throws CardSearchDefinitionException
     */
    public void addName( List<String> valueList, CardSearchOperator operator ) throws CardSearchDefinitionException
    {
        addName( valueList, operator, CardSearchLinkOperator.DEFAULT, 0 );
    }

    /**
     * @param valueList
     * @param operator
     * @param linkOperator
     * @throws CardSearchDefinitionException
     */
    public void addName( List<String> valueList, CardSearchOperator operator, CardSearchLinkOperator linkOperator )
                    throws CardSearchDefinitionException
    {
        addName( valueList, operator, linkOperator, 0 );
    }

    /**
     * @param valueList
     * @param operator
     * @param linkOperator
     * @param seachOrderNr
     * @throws CardSearchDefinitionException
     */
    public void addName( List<String> valueList, CardSearchOperator operator, CardSearchLinkOperator linkOperator,
                         int seachOrderNr ) throws CardSearchDefinitionException
    {
        addSearchValue( CardSearchAttribut.NAME, valueList, operator, linkOperator, seachOrderNr );
    }

}

/*
 *  Copyright 2016 Thomas Heynk
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package de.kartonubo.core.search;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.kartonubo.core.exception.CardSearchDefinitionException;
import de.kartonubo.core.repository.CardRepository;

/**
 * @author Thomas
 */
public class BaseCardSearchDefinition
{

    private static final Logger baseLogger = LoggerFactory.getLogger( BaseCardSearchDefinition.class );

    protected ArrayList<CardSearchValue> searchValueList;

    protected ArrayList<String> preparedValues;

    protected StringBuilder preparedStatement;

    protected StringBuilder orderBy;

    protected Date lastSqlBuild;

    protected Date lastUpdate;

    /**
     * 
     */
    public BaseCardSearchDefinition()
    {
        preparedValues = new ArrayList<>();
        searchValueList = new ArrayList<>();
        lastSqlBuild = new Date();
        lastUpdate = new Date();
    }

    //
    // Getter
    //

    /**
     * @return
     * @throws CardSearchDefinitionException
     */
    public List<String> getPreparedValues() throws CardSearchDefinitionException
    {
        createPreparedStatement();
        return preparedValues;
    }

    /**
     * @return
     * @throws CardSearchDefinitionException
     */
    public String getPreparedStatement() throws CardSearchDefinitionException
    {
        createPreparedStatement();
        return preparedStatement.toString();
    }

    // -------------------------------------------------------------------------------------
    //
    // Protected Methods
    //

    /**
     * @param attributName
     * @param valueList
     * @param operator
     * @param linkOperator
     * @param searchOrderNr
     * @throws CardSearchDefinitionException
     */
    protected void addSearchValue( CardSearchAttribut attributName, List<String> valueList, CardSearchOperator operator,
                                   CardSearchLinkOperator linkOperator, int searchOrderNr )
                                                   throws CardSearchDefinitionException
    {
        CardSearchValue searchValue =
            new CardSearchValue( attributName, valueList, operator, linkOperator, searchOrderNr );
        checkParameter( searchValue );
        searchValueList.add( searchValue );
        lastUpdate = new Date();
    }

    // -------------------------------------------------------------------------------------
    //
    // Private Methods
    //

    private void checkParameter( CardSearchValue searchValue ) throws CardSearchDefinitionException
    {
        switch ( searchValue.getAttributName() )
        {
            case ARTIFACT_ID:
                checkParameterArtifactId( searchValue );
                break;
            case COMMENT:
                break;
            case GROUP_ID:
                break;
            case LAST_UPDATE:
                break;
            case NAME:
                break;
            case TAG_LIST:
                break;
            case TEXT:
                break;
            default:
                break;
        }
    }

    private void checkParameterArtifactId( CardSearchValue searchValue ) throws CardSearchDefinitionException
    {
        checkEmptyValueList( searchValue );

        switch ( searchValue.getOperator() )
        {
            case DEFAULT:
            case EQUALS:
            case LIKE:
            case MATCHES:
            case NOT_EQUALS:
                sizeWarning( searchValue.getValueList().size(), "Mehr als ein Suchwert zum Operator '%s' definiert.",
                                searchValue.getOperator() );
                return;
            case IN:
                return;
            default:
                throw new CardSearchDefinitionException( "Ungültiger Operator '" + searchValue.getOperator()
                    + "' für Parameter 'ArtifactId'", CardSearchDefinitionException.OPERATOR_NOT_SUPPORTED,
                                                         searchValue );
        }
    }

    private void checkEmptyValueList( CardSearchValue searchValue ) throws CardSearchDefinitionException
    {
        if ( searchValue.getValueList().isEmpty() )
        {
            throw new CardSearchDefinitionException( "Leere ValueList für Parameter 'ArtifactId' definiert.",
                                                     CardSearchDefinitionException.EMPTY_VALUE_LIST, searchValue );
        }

    }

    private void sizeWarning( int value, String message, Object... args )
    {
        if ( value > 1 )
        {
            baseLogger.warn( String.format( message, args ) );
        }
    }

    // -------------------------------------------------------------------------------------

    /**
     * @throws CardSearchDefinitionException
     */
    private void createPreparedStatement() throws CardSearchDefinitionException
    {
        if ( lastUpdate.before( lastSqlBuild ) )
        {
            return;
        }
        preparedValues.clear();
        orderBy = null;
        orderBy = new StringBuilder();
        preparedStatement = null;
        preparedStatement = new StringBuilder();
        preparedStatement.append( "select from " ).append( CardRepository.ATTR_TYPE_NAME ).append( " where " );

        int counter = 0;
        for ( CardSearchValue searchValue : searchValueList )
        {
            addSearchValueToPreparedStatement( searchValue, counter++ );
        }
        lastSqlBuild = new Date();

    }

    private void addSearchValueToPreparedStatement( CardSearchValue searchValue, int counter )
                    throws CardSearchDefinitionException
    {
        if ( counter > 0 )
        {
            preparedStatement.append( searchValue.getLinkOperator() ).append( " " );
        }
        preparedStatement.append( searchValue.getAttributName() ).append( " " );
        preparedStatement.append( createOperatorExpression( searchValue ) );
    }

    private String createOperatorExpression( CardSearchValue searchValue ) // NOSONAR
                    throws CardSearchDefinitionException
    {
        switch ( searchValue.getOperator() )
        {
            case DEFAULT:
            case EQUALS:
            case GREATERTHAN:
            case GREATERTHAN_EQUAL:
            case LESSTHAN:
            case LESSTHAN_EQUAL:
            case LIKE:
                return createOperatorExpressionOneValue( searchValue );
            case IS_NULL:
                break;
            case BETWEEN:
                break;
            case IN:
                return createOperatorExpressionIN( searchValue );
            case MATCHES:
                break;
            case NOT_EQUALS:
                break;
            default:
                throw new CardSearchDefinitionException( "Unbekannter Operator '" + searchValue.getOperator() + "'",
                                                         CardSearchDefinitionException.OPERATOR_NOT_SUPPORTED,
                                                         searchValue );
        }
        return "";
    }

    private String createOperatorExpressionOneValue( CardSearchValue searchValue ) throws CardSearchDefinitionException
    {
        java.util.Iterator<String> iter = searchValue.getValueList().iterator();

        if ( iter.hasNext() )
        {
            preparedValues.add( iter.next() );
            return searchValue.getOperator().getOperator() + " ? ";
        }
        throw new CardSearchDefinitionException( "Leere ValueListe!", CardSearchDefinitionException.EMPTY_VALUE_LIST,
                                                 searchValue );
    }

    private String createOperatorExpressionIN( CardSearchValue searchValue ) throws CardSearchDefinitionException
    {
        java.util.Iterator<String> iter = searchValue.getValueList().iterator();

        if ( !iter.hasNext() )
        {
            throw new CardSearchDefinitionException( "Leere ValueListe!",
                                                     CardSearchDefinitionException.EMPTY_VALUE_LIST, searchValue );
        }

        StringBuilder strBuilder = new StringBuilder();

        preparedValues.add( iter.next() );
        strBuilder.append( searchValue.getOperator().getOperator() ).append( " [?" );

        while ( iter.hasNext() )
        {
            preparedValues.add( iter.next() );
            strBuilder.append( ",?" );
        }
        strBuilder.append( "] " );

        return strBuilder.toString();
    }
}

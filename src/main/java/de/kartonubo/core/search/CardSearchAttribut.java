/*
 *  Copyright 2016 Thomas Heynk
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package de.kartonubo.core.search;

import de.kartonubo.core.repository.CardRepository;

public enum CardSearchAttribut
{
    GROUP_ID( CardRepository.ATTR_GROUP_ID ),
    ARTIFACT_ID( CardRepository.ATTR_ARTIFACT_ID ),
    NAME( CardRepository.ATTR_NAME ),
    COMMENT( CardRepository.ATTR_COMMENT ),
    TEXT( CardRepository.ATTR_TEXT ),
    LAST_UPDATE( CardRepository.ATTR_LAST_UPDATE ),
    TAG_LIST( CardRepository.ATTR_TAG_LIST );

    private String attribut;

    private CardSearchAttribut( String attribut )
    {
        this.attribut = attribut;
    }

    public String getAttribut()
    {
        return attribut;
    }

    @Override
    public String toString()
    {
        return attribut;
    }
}

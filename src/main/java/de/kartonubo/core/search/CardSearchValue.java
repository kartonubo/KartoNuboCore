/*
 *  Copyright 2016 Thomas Heynk
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package de.kartonubo.core.search;

import java.util.ArrayList;
import java.util.List;

public class CardSearchValue
{

    private CardSearchAttribut attributName;

    private CardSearchOperator operator;

    private CardSearchLinkOperator linkOperator;

    private ArrayList<String> valueList;

    private int searchOrderNr;

    public CardSearchValue()
    {

        operator = CardSearchOperator.DEFAULT;
        linkOperator = CardSearchLinkOperator.DEFAULT;
        searchOrderNr = 0;
    }

    public CardSearchValue( CardSearchAttribut attributName, List<String> valueList, CardSearchOperator operator,
                    CardSearchLinkOperator linkOperator, int searchOrderNr )
    {
        this.attributName = attributName;
        this.operator = operator;
        this.linkOperator = linkOperator;
        this.valueList = new ArrayList<>();
        this.valueList.addAll( valueList );
        this.searchOrderNr = searchOrderNr;
    }

    public CardSearchAttribut getAttributName()
    {
        return attributName;
    }

    public void setAttributName( CardSearchAttribut attributName )
    {
        this.attributName = attributName;
    }

    public CardSearchOperator getOperator()
    {
        return operator;
    }

    public void setOperator( CardSearchOperator operator )
    {
        this.operator = operator;
    }

    public CardSearchLinkOperator getLinkOperator()
    {
        return linkOperator;
    }

    public void setLinkOperator( CardSearchLinkOperator linkOperator )
    {
        this.linkOperator = linkOperator;
    }

    public List<String> getValueList()
    {
        if ( valueList == null )
        {
            valueList = new ArrayList<>();
        }
        return valueList;
    }

    public void addValue( String value )
    {
        if ( valueList == null )
        {
            valueList = new ArrayList<>();
        }
        valueList.add( value );
    }

    public int getSearchOrderNr()
    {
        return searchOrderNr;
    }

    public void setSeachrOrderNr( int searchOrderNr )
    {
        this.searchOrderNr = searchOrderNr;
    }

}

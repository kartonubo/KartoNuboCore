/*
 *  Copyright 2016 Thomas Heynk
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package de.kartonubo.core.search;

public enum CardSearchOperator
{

    // Achtung: in alle Funktionen wird davon ausgegangen, dass DEFAULT == EQUALS ist !

    DEFAULT( "=" ),
    EQUALS( "=" ),
    NOT_EQUALS( "<>" ),
    LESSTHAN( "<" ),
    LESSTHAN_EQUAL( "<=" ),
    GREATERTHAN( ">" ),
    GREATERTHAN_EQUAL( ">=" ),
    BETWEEN( "BETWEEN" ),
    IN( "IN" ),
    IS_NULL( "IS NULL" ),
    MATCHES( "MATCHES" ),
    LIKE( "LIKE" );

    private String operator;

    private CardSearchOperator( String operator )
    {
        this.operator = operator;
    }

    public String getOperator()
    {
        return operator;
    }

    @Override
    public String toString()
    {
        return operator;
    }
}

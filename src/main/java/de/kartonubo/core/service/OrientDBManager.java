/*
 *  Copyright 2016 Thomas Heynk
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package de.kartonubo.core.service;

import com.tinkerpop.blueprints.impls.orient.OrientGraph;
import com.tinkerpop.blueprints.impls.orient.OrientGraphFactory;
import com.tinkerpop.blueprints.impls.orient.OrientGraphNoTx;

import de.kartonubo.core.exception.OrientDBManagerException;
import de.kartonubo.core.exception.ValueValidateException;

import org.apache.commons.lang.StringUtils;

/**
 * @author Thomas
 */
public class OrientDBManager
{

    private String databaseURL = null;

    private OrientGraphFactory factory = null;

    private final ConfigurationManager configuration;

    /**
     * 
     */
    public OrientDBManager()
    {

        configuration = ConfigurationManager.getInstance();
    }

    /**
     * @throws ValueValidateException
     */
    public void openFactory() throws OrientDBManagerException
    {

        openFactory( configuration.getOrientDatabaseURL() );
    }

    /**
     * @param dbUrl
     * @return
     * @throws OrientDBManagerException
     */
    private void openFactory( String dbUrl ) throws OrientDBManagerException
    {

        if ( StringUtils.isBlank( dbUrl ) )
        {
            throw new OrientDBManagerException( "DatabaseURL" + " '" + dbUrl + "' nicht DEFINIERT!",
                                                OrientDBManagerException.URL_IS_BLANK );
        }
        if ( databaseURL == null )
        {
            newFactory( dbUrl );
            return;
        }
        if ( databaseURL.equals( dbUrl ) && factory == null )
        {
            newFactory( dbUrl );
        }
    }

    /**
     * @param dbUrl
     */
    private void newFactory( String dbUrl )
    {

        closeFactory();
        databaseURL = dbUrl;
        factory = new OrientGraphFactory( databaseURL ).setupPool( 1, 10 );
    }

    /**
     * 
     */
    public void closeFactory()
    {

        if ( isFactoryOpen() )
        {
            factory.close();
            factory = null;
        }
    }

    public boolean isFactoryOpen()
    {

        return factory != null;
    }

    /**
     * @return
     * @throws OrientDBManagerException
     */
    public OrientGraph createGraph() throws OrientDBManagerException
    {

        if ( isFactoryOpen() )
        {
            return factory.getTx();
        }
        throw new OrientDBManagerException( "OrientGraph konnte nicht erstellt werden, weil Factory nicht initialisiert!",
                                            OrientDBManagerException.FACTORY_NOT_OPEN );
    }

    public OrientGraphNoTx createGraphWithNoTx() throws OrientDBManagerException
    {

        if ( isFactoryOpen() )
        {
            return factory.getNoTx();
        }
        throw new OrientDBManagerException( "OrientGraph konnte nicht erstellt werden, weil Factory nicht initialisiert!",
                                            OrientDBManagerException.FACTORY_NOT_OPEN );
    }

}

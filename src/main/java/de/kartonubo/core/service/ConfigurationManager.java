/*
 *  Copyright 2016 Thomas Heynk
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package de.kartonubo.core.service;

/**
 * @author Thomas
 */
public class ConfigurationManager
{

    private static final String REMOTE_URL = "remote:localhost/tomsDB";

    private static final String LOCAL_URL = "plocal:./OrientDB/localOrientTestDB";

    private static final String MEMORY_URL = "memory:kartoNubo";

    private static ConfigurationManager instance = null;

    private String dbUrl = MEMORY_URL;

    protected ConfigurationManager()
    {
    }

    /**
     * @return
     */
    public static ConfigurationManager getInstance()
    {
        if ( instance == null )
        {
            instance = new ConfigurationManager();
        }
        return instance;
    }

    /**
     * @return
     */
    public String getOrientDatabaseURL()
    {
        return dbUrl;
    }

    public void setOrientDatabaseURLtoMEMORY()
    {
        dbUrl = MEMORY_URL;
    }

    public void setOrientDatabaseURLtoLOCAL()
    {
        dbUrl = LOCAL_URL;
    }

    public void setOrientDatabaseURLtoREMOTE()
    {
        dbUrl = REMOTE_URL;
    }

}

/*
 *  Copyright 2016 Thomas Heynk
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package de.kartonubo.core.data;

import de.kartonubo.core.exception.ValueValidateException;
import java.io.Serializable;
import java.util.Date;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;

/**
 * ----- TODO ------
 * <table summary="">
 * <tr>
 * <th>Attribute</th>
 * <th>Description</th>
 * </tr>
 * <tr>
 * <td>GroupId (required)</td>
 * <td>Bedeutung wie im Buildsystem Maven. Bildet mit der ArtefactId zusammen den eindeutigen Schlüssel.</td>
 * </tr>
 * <tr>
 * <td>ArtifactID (required)</td>
 * <td>Bedeutung wie im Buildsystem Maven. Bildet mit der GroupId zusammen den eindeutigen Schlüssel.</td>
 * </tr>
 * <tr>
 * <td>Name</td>
 * <td>Name/Bezeichnung der Karte, die von der ArtifactID abweichen kann. DefaultWert: ""</td>
 * </tr>
 * <tr>
 * <td>Text</td>
 * <td>Der eigentliche textuelle Inhalt. DefaultWert: ""</td>
 * </tr>
 * <tr>
 * <td>Comment</td>
 * <td>Kommentare, Anmerkungen. DefaultWert: ""</td>
 * </tr>
 * <tr>
 * <td>TagList</td>
 * <td>Enthält eine Liste von Tags. Jeder Tag beginnt und endet mit dem Delimiter '|'. Beispiel: "|Java||Insel||klein|"
 * DefaultWert: ""</td>
 * </tr>
 * <tr>
 * <td>LastUpdate</td>
 * <td>Datum der letzten Änderung. DefaultWert: Datum der ersten Erstellung (new Card(...))</td>
 * </tr>
 * </table>
 * 
 * @author Thomas
 */
public class Card implements Serializable
{

    private static final long serialVersionUID = 1L;

    public static final String TAG_DELIMITER = "|";

    public static final String REGEX_GROUP_ID = "^([a-z][a-z._-]*[a-z])$";

    public static final String REGEX_ARTIFACT_ID = "^([a-z][a-zA-Z0-9_-]*[a-z0-9])$";

    public static final String REGEX_TAG = "^([\\p{Alpha}][\\p{Alnum}_-]*[\\p{Alnum}])$";

    public static final String EMPTY_STRING = "";

    private String groupId;

    private String artifactId;

    private String name;

    private String comment;

    private String text;

    private String tagList;

    private Date lastUpdate;

    /**
     * @param groupId
     * @param artifactId
     * @throws ValueValidateException
     */
    public Card( String groupId, String artifactId ) throws ValueValidateException
    {

        setGroupId( groupId );
        setArtifactId( artifactId );
        name = EMPTY_STRING;
        comment = EMPTY_STRING;
        text = EMPTY_STRING;
        tagList = EMPTY_STRING;
        setLastUpdate();
    }

    // ----------------------------------------------------------------
    // * GroupId

    public final String getGroupId()
    {
        return groupId;
    }

    public final void setGroupId( String value ) throws ValueValidateException
    {
        checkGroupId( value );
        groupId = value;
    }

    public static final void checkGroupId( String value ) throws ValueValidateException
    {
        checkValue( value, "GroupId", REGEX_GROUP_ID, 0 );
    }

    public static final boolean isGroupIdValid( String value )
    {
        try
        {
            checkGroupId( value );
        }
        catch ( ValueValidateException ex ) // NOSONAR
        {
            return false;
        }
        return true;
    }

    // ----------------------------------------------------------------
    // * ArtifactId

    public final String getArtifactId()
    {
        return artifactId;
    }

    public final void setArtifactId( String value ) throws ValueValidateException
    {
        checkArtifactId( value );
        artifactId = value;
    }

    public static final void checkArtifactId( String value ) throws ValueValidateException
    {
        checkValue( value, "ArtifactId", REGEX_ARTIFACT_ID, 0 );
    }

    public static final boolean isArtifactIdValid( String value )
    {
        try
        {
            checkArtifactId( value );
        }
        catch ( ValueValidateException ex ) // NOSONAR
        {
            return false;
        }
        return true;
    }

    // ----------------------------------------------------------------
    // * Name

    public final String getName()
    {
        return name;
    }

    public final void setName( String value )
    {
        name = setStringValue( value );
    }

    // ----------------------------------------------------------------
    // * Comment

    public final String getComment()
    {
        return comment;
    }

    public final void setComment( String value )
    {
        comment = setStringValue( value );
    }

    // ----------------------------------------------------------------
    // * Text

    public final String getText()
    {
        return text;
    }

    public final void setText( String value )
    {
        text = setStringValue( value );
    }

    // ----------------------------------------------------------------
    // * TagList

    public final String getTagList()
    {
        return tagList;
    }
    
    public final String[] getTagListArray()
    {
        if ( StringUtils.isBlank( tagList ) )
        {
            return new String[0];
        }
        return  StringUtils.split( tagList, TAG_DELIMITER );
    }


    public final void setTagList( String value ) throws ValueValidateException
    {
        if ( StringUtils.isBlank( value ) )
        {
            tagList = EMPTY_STRING;
            return;
        }
        String[] strArr = StringUtils.split( tagList, TAG_DELIMITER );

        String newTagList = null;

        for ( int i = 0; i < strArr.length; i++ )
        {
            newTagList = addTagToList( strArr[i], newTagList );
        }
        tagList = newTagList;
    }

    // ----------------------------------------------------------------
    // * LastUpdate

    public final Date getLastUpdate()
    {
        return lastUpdate;
    }

    public final void setLastUpdate( Date value )
    {
        if ( value == null )
        {
            setLastUpdate();
            return;
        }
        lastUpdate = value;
    }

    public final void setLastUpdate()
    {
        lastUpdate = new Date();
    }

    // ----------------------------------------------------------------
    // * Tag

    public final void addTag( String value ) throws ValueValidateException
    {
        tagList = addTagToList( value, tagList );
    }

    public final String addTagToList( String tagValue, String list ) throws ValueValidateException
    {
        checkTag( tagValue );
        String newTag = StringUtils.trim( tagValue );
        if ( isTagDefined( newTag, list ) )
        {
            return list;
        }
        if ( StringUtils.isBlank( list ) )
        {
            return addTagDelimiter( newTag );
        }
        return list + addTagDelimiter( newTag );
    }

    public final void removeTag( String value ) throws ValueValidateException
    {
        checkTag( value );
        String oldTag = StringUtils.trim( value );
        if ( isTagDefined( oldTag ) )
        {
            tagList = StringUtils.remove( tagList, addTagDelimiter( oldTag ) );
        }
    }

    public final boolean isTagDefined( String value ) throws ValueValidateException
    {
        return isTagDefined( value, tagList );
    }

 
    public static final void checkTag( String value ) throws ValueValidateException
    {
        checkValue( value, "Tag", REGEX_TAG, Pattern.UNICODE_CHARACTER_CLASS );
    }

    public static final boolean isTagValid( String value )
    {
        try
        {
            checkTag( value );
        }
        catch ( ValueValidateException ex ) // NOSONAR
        {
            return false;
        }
        return true;
    }

    //
    // Private Methods
    //

    private String addTagDelimiter( String value )
    {
        return TAG_DELIMITER + value + TAG_DELIMITER;
    }

    private static void checkValue( String value, String valueName, String pattern, int flags )
                    throws ValueValidateException
    {
        if ( StringUtils.isBlank( value ) )
        {
            throw new ValueValidateException( valueName + " '" + value + "' nicht DEFINIERT!",
                                              ValueValidateException.VALUE_IS_BLANK );
        }
        if ( !validateValue( value, pattern, flags ) )
        {
            throw new ValueValidateException( "Tag '" + value + "' entspricht nicht dem Pattern:'" + pattern + "'",
                                              ValueValidateException.VALUE_WRONG_PATTERN );
        }
    }

    private static boolean validateValue( String value, String pattern, int flags )
    {
        if ( flags > 0 )
        {
            return Pattern.compile( pattern, flags ).matcher( value ).matches();
        }
        return value.matches( pattern );
    }

    private String setStringValue( String value )
    {
        if ( StringUtils.isBlank( value ) )
        {
            return EMPTY_STRING;
        }
        return value;
    }

    private final boolean isTagDefined( String value, String valueList ) throws ValueValidateException
    {
        checkTag( value );
        String searchStr = addTagDelimiter( StringUtils.trim( value ) );
        return StringUtils.indexOf( valueList, searchStr ) != -1;
    }
    
}

/*
 *  Copyright 2016 Thomas Heynk
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package de.kartonubo.core.exception;

/**
 * @author Thomas
 */
public class RepositoryException extends Exception
{

    private static final long serialVersionUID = 1L;

    public static final String CARD_EXISTS = "CardExistsInDB";

    public static final String CARD_NOT_EXISTS = "CardNotExistsInDB";

    public static final String MULTIPLE_CARDS = "MoreThanOneCard";

    public static final String CARDID_NOT_CREATED = "CardIdNotCreated";

    public static final String CARD_NOT_DEFINED = "CardNotDefined";

    public static final String DB_EXCEPTION = "DBException";

    public static final String DB_MANAGER_EXCEPTION = "DBManagerException";

    public static final String DB_INVALID_CARD_VALUES = "DBInvalidCardValues";

    public static final String CARD_SEARCH_EXCEPTION = "CardSearchException";

    public static final String TYP_CARD_NOT_DEFINED = "TypeCardNotDefined";

    public static final String ORIENT_DB_MANAGER_EX = "OrientDBManagerException";

    private final String type;

    public RepositoryException( String s, String type )
    {
        super( s );
        this.type = type;
    }

    public RepositoryException( String s, String type, Exception ex )
    {
        super( s, ex );
        this.type = type;
    }

    public String getType()
    {
        return type;
    }

}

/*
 *  Copyright 2016 Thomas Heynk
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package de.kartonubo.core.exception;

/**
 * @author Thomas
 */
public class ValueValidateException extends Exception
{

    private static final long serialVersionUID = 1L;

    public static final String VALUE_IS_BLANK = "ValueIsBlank";

    public static final String VALUE_WRONG_PATTERN = "ValueDoesNoMatchThePattern";

    private final String type;

    public ValueValidateException( String s, String type )
    {
        super( s );
        this.type = type;
    }

    public ValueValidateException( String s, String type, Exception ex )
    {
        super( s, ex );
        this.type = type;
    }

    public String getType()
    {
        return type;
    }

}

/*
 *  Copyright 2016 Thomas Heynk
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package de.kartonubo.core.exception;

/**
 * @author Thomas
 */
public class OrientDBManagerException extends Exception
{

    private static final long serialVersionUID = 1L;

    public static final String URL_IS_BLANK = "UrlIsBlank";

    public static final String FACTORY_NOT_OPEN = "FactroyNotOpen";

    private final String type;

    public OrientDBManagerException( String s, String type )
    {
        super( s );
        this.type = type;
    }

    public OrientDBManagerException( String s, String type, Exception ex )
    {
        super( s, ex );
        this.type = type;
    }

    public String getType()
    {
        return type;
    }

}

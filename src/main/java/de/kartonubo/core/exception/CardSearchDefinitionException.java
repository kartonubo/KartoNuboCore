/*
 *  Copyright 2016 Thomas Heynk
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package de.kartonubo.core.exception;

import de.kartonubo.core.search.CardSearchValue;

public class CardSearchDefinitionException extends Exception
{

    private static final long serialVersionUID = 1L;

    public static final String OPERATOR_NOT_SUPPORTED = "OperatorNotSupported";

    public static final String EMPTY_VALUE_LIST = "EmptyValueList";

    private final String type;

    private final CardSearchValue searchValue;

    public CardSearchDefinitionException( String s, String type, CardSearchValue searchValue )
    {
        super( s );
        this.type = type;
        this.searchValue = searchValue;
    }

    public CardSearchDefinitionException( String s, String type, CardSearchValue searchValue, Exception ex )
    {
        super( s, ex );
        this.type = type;
        this.searchValue = searchValue;
    }

    public String getType()
    {
        return type;
    }

    public CardSearchValue getSearchValue()
    {
        return searchValue;
    }

}

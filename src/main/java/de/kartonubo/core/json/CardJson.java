/*
 *  Copyright 2016 Thomas Heynk
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package de.kartonubo.core.json;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import org.apache.commons.lang3.StringUtils;
import org.hjson.Stringify;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonValue;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.WriterConfig;

import de.kartonubo.core.data.Card;
import de.kartonubo.core.exception.JsonParseException;
import de.kartonubo.core.exception.ValueValidateException;
import de.kartonubo.core.repository.CardRepository;

public class CardJson
{
    
    public static final String JSON_ATTR_CLASS = "class";

    public static final String JSON_CLASS_NAME = CardRepository.ATTR_TYPE_NAME;

    public static final String JSON_ATTR_GROUP_ID = CardRepository.ATTR_GROUP_ID;

    public static final String JSON_ATTR_ARTIFACT_ID = CardRepository.ATTR_ARTIFACT_ID;

    public static final String JSON_ATTR_NAME =  CardRepository.ATTR_NAME;

    public static final String JSON_ATTR_COMMENT = CardRepository.ATTR_COMMENT;

    public static final String JSON_ATTR_TEXT = CardRepository.ATTR_TEXT;

    public static final String JSON_ATTR_TAG_LIST = CardRepository.ATTR_TAG_LIST;
    
    public static final String JSON_ATTR_LAST_UPDATE = CardRepository.ATTR_LAST_UPDATE;

    private boolean humanJson;

    private boolean prettyPrint;

    private boolean excludeName;

    private boolean excludeText;

    private boolean excludeComment;

    private String dateFormat;

    public CardJson()
    {
        setHumanJson( false );
        setPrettyPrint( false );
        setExcludeName( false );
        setExcludeText( false );
        setExcludeComment( false );
        setDateFormat( "yyy-MM-dd hh:mm:ss.SSS" );
    }

    /**
     * @param card
     * @return
     */
    public String encode( Card card )
    {

        JsonObject jsonCard = Json.object().add( JSON_ATTR_CLASS, JSON_CLASS_NAME );
        jsonCard.add( JSON_ATTR_ARTIFACT_ID, card.getArtifactId() );
        jsonCard.add( JSON_ATTR_GROUP_ID, card.getGroupId() );
        // name
        if ( !isExcludeName() )
        {
            jsonCard.add( JSON_ATTR_NAME, card.getName() );
        }
        // text
        if ( !isExcludeText() )
        {
            jsonCard.add( JSON_ATTR_TEXT, card.getText() );
        }
        // tagList
        String[] tagList = card.getTagListArray();
        if ( tagList.length > 0 )
        {
            jsonCard.add( JSON_ATTR_TAG_LIST, createJsonTagListArray( tagList ) );
        }
        // comment
        if ( !isExcludeComment() )
        {
            jsonCard.add( JSON_ATTR_COMMENT, card.getComment() );
        }
        // lastUpdate
        Date lastUpdate = card.getLastUpdate();
        if ( lastUpdate != null )
        {
            jsonCard.add( JSON_ATTR_LAST_UPDATE, lastUpdateToString( lastUpdate ) );
        }

        // Return JsonString
        if ( isHumanJson() )
        {
            return org.hjson.JsonValue.readHjson( jsonCard.toString() ).toString( Stringify.HJSON );
        }
        else if ( isPrettyPrint() )
        {
            return jsonCard.toString( WriterConfig.PRETTY_PRINT );
        }
        return jsonCard.toString();
    }

    /**
     * @param json
     * @return
     * @throws JsonParseException
     */
    public Card decode( String json ) throws JsonParseException
    {
        JsonObject cardJsonObj = createJsonObj( json );

        Card card = createCard( cardJsonObj );

        return addJasonValuesToCard( card, cardJsonObj );
    }

    //
    // Getter and Setter
    //

    public boolean isHumanJson()
    {
        return humanJson;
    }

    public void setHumanJson( boolean humanJson )
    {
        this.humanJson = humanJson;
    }

    public boolean isPrettyPrint()
    {
        return prettyPrint;
    }

    public void setPrettyPrint( boolean prettyPrint )
    {
        this.prettyPrint = prettyPrint;
    }

    public boolean isExcludeName()
    {
        return excludeName;
    }

    public void setExcludeName( boolean excludeName )
    {
        this.excludeName = excludeName;
    }

    public boolean isExcludeText()
    {
        return excludeText;
    }

    public void setExcludeText( boolean excludeText )
    {
        this.excludeText = excludeText;
    }

    public boolean isExcludeComment()
    {
        return excludeComment;
    }

    public void setExcludeComment( boolean excludeComment )
    {
        this.excludeComment = excludeComment;
    }

    public String getDateFormat()
    {
        return dateFormat;
    }

    public void setDateFormat( String dateFormat )
    {
        this.dateFormat = dateFormat;
    }

    //
    // Private Methods
    //

    private JsonArray createJsonTagListArray( String[] tagList )
    {
        JsonArray jsonArr = new JsonArray();
        for ( int i = 0; i < tagList.length; i++ )
        {
            jsonArr.add( tagList[i] );
        }
        return jsonArr;

    }

    private String lastUpdateToString( Date lastUpdate )
    {
        String strDate;

        if ( getDateFormat() == null )
        {
            strDate = Long.toString( lastUpdate.getTime() );
        }
        else
        {
            DateFormat dfmt = new SimpleDateFormat( getDateFormat() );
            strDate = dfmt.format( lastUpdate );
        }
        return strDate;
    }

    private Card addTagListToCard( Card card, JsonArray tagArray ) throws ValueValidateException
    {
        Iterator<JsonValue> iter = tagArray.iterator();

        while ( iter.hasNext() )
        {
            JsonValue value = iter.next();
            card.addTag( value.asString() );
        }

        return card;
    }

    private JsonObject createJsonObj( String json ) throws JsonParseException
    {
        String jsonString = isHumanJson() ? org.hjson.JsonValue.readHjson( json ).toString() : json;

        JsonObject cardJsonObj = Json.parse( jsonString ).asObject();
        if ( cardJsonObj == null )
        {
            throw new JsonParseException( "Invalid JsonString: \"" + jsonString + "\"",
                                          JsonParseException.JSON_OBJECT_NOT_FOUND );
        }
        return cardJsonObj;
    }

    private Card createCard( JsonObject cardObj ) throws JsonParseException
    {
        String className = cardObj.get( JSON_ATTR_CLASS ).asString();
        if ( !JSON_CLASS_NAME.equalsIgnoreCase( className ) )
        {
            throw new JsonParseException( "ClassName not defined!", JsonParseException.CLASS_NOT_DEFINED );
        }

        String artifactId = cardObj.get( JSON_ATTR_ARTIFACT_ID ).asString();
        if ( StringUtils.isBlank( artifactId ) )
        {
            throw new JsonParseException( "ArtifactId not defined!", JsonParseException.ARTIFACTID_NOT_DEFINED );
        }

        String groupId = cardObj.get( JSON_ATTR_GROUP_ID ).asString();
        if ( StringUtils.isBlank( artifactId ) )
        {
            throw new JsonParseException( "GroupId not defined!", JsonParseException.GROUPID_NOT_DEFINED );
        }

        
        try
        {
            return new Card( groupId, artifactId ); 
        }
        catch ( ValueValidateException e )
        {
            throw new JsonParseException( "Card not created!", JsonParseException.VALUE_VALIDATE_EXCEPTION, e );
        }
    }

    private Card addJasonValuesToCard( Card card, JsonObject cardJsonObj ) throws JsonParseException
    {
        card.setName( cardJsonObj.get( JSON_ATTR_NAME ).asString() );
        card.setText( cardJsonObj.get( JSON_ATTR_TEXT ).asString() );
        card.setComment( cardJsonObj.get( JSON_ATTR_COMMENT ).asString() );

        JsonArray tagArray = cardJsonObj.get( JSON_ATTR_TAG_LIST ).asArray();

        try
        {
            return addTagListToCard( card, tagArray );
        }
        catch ( ValueValidateException e )
        {
            throw new JsonParseException( "Wrong Value in TagList!", JsonParseException.VALUE_VALIDATE_EXCEPTION, e );
        }
    }

}
